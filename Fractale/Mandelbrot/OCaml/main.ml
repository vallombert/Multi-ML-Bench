
let mandelbrot xMin xMax yMin yMax maxIter xPixels yPixels img =
  let rec mandelbrotIterator z c n =
    if (Complex.norm z) > 2.0 then false else
      match n with
      | 0 -> true
      | n -> let z' = Complex.add (Complex.mul z z) c in
             mandelbrotIterator z' c (n-1) in
  let dx = (xMax -. xMin) /. (float_of_int xPixels)
  and dy = (yMax -. yMin) /. (float_of_int yPixels) in
  for xi = 0 to xPixels - 1 do
    for yi = 0 to yPixels - 1 do
      let c = {Complex.re = xMin +. (dx *. float_of_int xi);
               Complex.im = yMin +. (dy *. float_of_int yi)} in
      if (mandelbrotIterator Complex.zero c maxIter) then
        Rgb24.set img xi yi { Color.r = 0; g = 0; b = 255 }
      else
        ()(* Rgb24.set img xi yi { Color.r = 0; g = 0; b = 0 } *)
    done
  done;;

let _ =
  let xPixels = 2000 in
  let yPixels  = xPixels in  
  let img = Rgb24.create xPixels yPixels in
  let start = Unix.gettimeofday () in
  let _ = mandelbrot (-1.5) 0.5 (-1.0) 1.0 200 xPixels yPixels img in
  let stop1 = Unix.gettimeofday () in
  let _ = Printf.printf "Max computation time: %fs\n" (stop1 -. start) in
  Jpeg.save "out.jpg" [] (Images.Rgb24 img)

