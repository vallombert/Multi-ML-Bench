#!/bin/bash

outfile=$1
nb_it=$2
hostfile=$3
exec=$4

if [ $# != 4 ] ; then
    echo "Missing args bitch : outfile nb_it hostfile exec"
else

    echo "Let's fu***** do this ! "
    echo "cmd: " $exec > $outfile

    for ((i=0 ; nb_it - $i ; i++)) do
	echo "run $i"
	echo "run $i" >> $outfile
	mpirun --mca btl_tcp_if_include eth0 -hostfile $hostfile $exec >> $outfile
    done

	echo " ---------------- " >> $outfile
	cat $outfile | grep "Execution time" | awk '{total += $4; count++} END {print "Total execution time = " total/count}'  >> $outfile
	echo " ---------------- " >> $outfile

	cat $outfile

    fi
