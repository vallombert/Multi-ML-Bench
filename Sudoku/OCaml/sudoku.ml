type element = {
    value: int;
    hyp: int list ref;
  }
type grid = element array
type sudoku = {
    grid: grid;
    size: int;
  }

let rec from_to n1 n2 =
  if n1>n2 then [] else n1::(from_to (n1+1) n2)

                              
let mk_grid n vals =
  if (n <> (int_of_float (sqrt(float_of_int (Array.length vals))))) then
    failwith "wrong size"
  else
    begin
      let g =
        {
          grid=Array.init (n*n)
                          (fun x ->
                            {
                              value=vals.(x);
                              hyp= ref (from_to 0 (n))
                            }
                          );
          size=n;
        }
      in
      let _ = Array.iteri (fun i x -> 
                  if x.value <> 0 then
                    begin
                      g.grid.(i).hyp := []
                    end
                  else 
                    g.grid.(i).hyp := (from_to 0 (n)) 
                )
                          g.grid
      in g
    end

let at (g:sudoku) r c =
  g.grid.(r*g.size+c)

let display g =
  let n = g.size in
  for i = 0 to n-1 do
    for j = 0 to n-1 do
      Printf.printf "%i " (at g i j).value
    done;
    Printf.printf "\n"
  done


let to_eliminate_row (g:sudoku) (r:int) =
  let rec aux g r id l =
    if id = g.size then
      l
    else
      if (at g r id).value <> 0 then
        aux g r (id+1) ([(at g r id).value]@l)
      else
        aux g r (id+1) l
  in aux g r 0 []

let to_eliminate_col (g:sudoku) (c:int) =
  let rec aux g r id l =
    if id = g.size then
      l
    else
      if (at g id c).value <> 0 then
        aux g c (id+1) ([(at g id c).value]@l)
      else
        aux g c (id+1) l
  in aux g c 0 []
         
(*Possibility List at g[row][col]*)
let possibility_list g r c =
  if (at g r c).value <> 0 then
    [(at g r c).value]
  else
    begin
      let to_elim = (to_eliminate_row g r)@(to_eliminate_col g c) in
      ((at g r c).hyp) := List.filter (fun x -> if List.mem x to_elim then false else true) !((at g r c).hyp); []
    end
