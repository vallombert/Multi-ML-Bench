open Bsml

let generate size sub_size md =
  (* let rec generate size lst = *)
  (*   if size = 0 then *)
  (*     lst *)
  (*   else *)
  (*     generate (size-1) ((size mod md)::lst) *)
  (* in generate size lst *)
  Array.to_list (Array.init size (fun i -> Array.to_list (Array.init sub_size (fun i -> i mod md))))

let max_of exectime nprocs =
  let pr = proj exectime in
  let restime = Array.init (nprocs) (fun i -> pr i) in
  Array.fold_left (max) (Array.get restime 0) restime


(**)

let rec from_to n1 n2 = if n1>n2 then [] else n1::(from_to (n1+1) n2)

let procs = from_to 0 (bsp_p-1)

let this = mkpar(fun pid->pid) 

let id x = x
             
let compose f g x = f (g x)
                      
let noSome (Some x) = x
                        
let replicate x = mkpar (fun pid->x)
                        
let parfun f v = apply (replicate f) v

let parfun2 f v1 v2 = apply (parfun f v1) v2

let apply2 f v1 v2 = apply (apply f v1) v2

let apply3 f v1 v2 v3 = apply (apply2 f v1 v2) v3

let rec drop n l = 
  if n<=0 
  then l 
  else drop (n-1) (List.tl l) 

let rec take_every n stride = 
  function
    []->[]
   |(h::t) ->
     if n<=1 
     then 
       if n<1 then [] else [h]
     else  
       h::(take_every (n-1) stride (drop (stride-1) t))

            
let put_one dst_and_datas = 
  let msgs = 
    parfun 
      (fun (dst,data) dstpid ->if dstpid=dst then Some data else None)
      dst_and_datas in
  let rec to_list l f = match l with
      [] -> []
    | h::t -> match (f h) with 
	        Some x -> x::(to_list t f) 
	       |	None -> (to_list t f) in
  parfun (to_list procs) (put msgs) 


let put_list ldst_and_datas =
  let msgs = parfun 
               (fun list dst-> 
	         try Some (List.assoc dst list)
	         with Not_found -> None)  
               ldst_and_datas in
  let rec to_list l f = match l with
      [] -> []
    | h::t -> match (f h) with 
	        Some x -> x::(to_list t f) 
	       |	None -> (to_list t f) in
  parfun (to_list procs) (put msgs) 


let rec takecond cond = 
  function
    [] -> ([],[])
  | (h::t) as l-> 
     if  cond h 
     then 
       let (l1,l2) = (takecond cond t) in (h::l1,l2)
     else ([],l)

            
let partition_list compare pivots list =
  let rec aux i pivots list = 
    if pivots = [] 
    then [i,list]
    else
      let cond = fun x -> compare x (List.hd pivots) in
      match list with
      	[] -> []
       |	list -> 
		 let ll = takecond cond list in 
		 (i,(fst ll))::(aux (i+1) (List.tl pivots) (snd ll)) in
  aux 0 pivots list

      
let totex vv = 
  parfun (compose noSome) (put(parfun (fun v dst->Some v) vv))

let total_exchange vec =
  parfun2 List.map (totex vec) (replicate procs)


exception Scatter

let scatter partition root v =
  if not (within_bounds root) 
  then raise Scatter
  else
    let mkmsg = mkpar(fun pid->if pid=root 
			       then partition else fun v _->None) in
    let msg = put (apply mkmsg v) in
    parfun noSome (apply msg (replicate root))



let cut_array_or_string length sub x = 
  let totlen = length x in 
  let len  = totlen / bsp_p 
  and nb = totlen mod bsp_p in
  fun i -> if i<nb
           then Some (sub x (i*(len+1)) (len+1))
           else Some (sub x (nb*(len+1)+(i-nb)*len) len)
	             
                     
exception Bcast
            
(* let bcast_totex_gen partition paste root vv = *)
(*   if not (within_bounds root) then raise Bcast else *)
(*     let phase1 = scatter partition root vv in *)
(*     let phase2 = totex phase1 in *)
(*     parfun paste phase2 *)

            
(* let bcast_totex_string root vs = *)
(*   let paste f = String.concat "" (List.map f procs) in *)
(*   bcast_totex_gen  *)
(*     (cut_array_or_string String.length String.sub)  *)
(*     paste root vs *)
            
(* let bcast_totex root vv =  *)
(*   let input_data = parfun (fun v->Marshal.to_string v [Marshal.Closures]) vv in *)
(*   let output_data = bcast_totex_string root input_data in *)
(*   parfun2 (Marshal.from_string) output_data (replicate 0) *)

let bcast_direct root vv = 
  if not (within_bounds root) then raise Bcast else
    let mkmsg = mkpar(fun pid v dst->if pid=root then Some v else None) in
    parfun noSome (apply (put (apply mkmsg vv)) (replicate root))


exception Regular_sampling_sort
            
let regular_sampling_sort_list (compare:'a -> 'a -> bool) (vec:'a list par) =
  let p = bsp_p and int = int_of_float and float = float_of_int in
  (* val localy_sorted : 'a list Bsmlcore.par *)
  let localy_sorted = parfun (Sort.list compare) vec in
  (* val local_length :int list Bsmlcore.par *)
  let local_length = total_exchange ((parfun List.length) vec) in
  (* val global_length : int Bsmlcore.par *)
  let global_length = 
    parfun 
      (fun l -> let gl = List.fold_left (+) 0 l in 
	        if gl<p*p then raise Regular_sampling_sort else gl) 
      local_length in
  (* val sample_stride : int Bsmlcore.par *)
  let sample_stride = 
    parfun (fun gl -> int((float gl)/.(float(p*p)))) global_length 
  (* val number_of_samples : int Bsmlcore.par *)
  and number_of_samples = 
    (* val approximation :int list Bsmlcore.par *)
    let approximation = 
      apply2 
	(replicate 
	   (fun gl ll->
	     List.map
	       (fun len -> int ((float(len*p*p))/.((float gl)-.0.5)))
	       ll))
	global_length 
	local_length in 
    (* val approximation_sum :int Bsmlcore.par *)
    let approximation_sum = 
      parfun (List.fold_left (+) 0) approximation in
    apply2
      (mkpar(fun pid sum ap->(List.nth ap pid)+(if pid<p*p-sum then 1 else 0)))
      approximation_sum
      approximation in
  (* val sent_samples : 'a list Bsmlcore.par*)
  let sent_samples = 
    put_one
      (apply3 
	 (replicate (fun nos ss ls -> 0,take_every nos ss ls))
	 number_of_samples
	 sample_stride
	 localy_sorted) in
  (* val sorted_samples : 'a list Bsmlcore.par *)
  let sorted_samples = 
    parfun (Sort.list compare) (parfun (List.fold_left (@) []) sent_samples) in
  (* val regular_pivots : 'a list Bsmlcore.par *)
  let regular_pivots =
    bcast_direct 
      0 
      (apply
	 (mkpar (function 0 ->take_every (p-1) p | _ ->  id))
	 (apply
	    (mkpar (function 0 ->drop ((p-2)+p/2) | _ -> id)) 
	    sorted_samples)) in 
  (* val distributed_partitions : 'a list list  Bsmlcore.par *)
  let distributed_partitions = 
    put_list
      (apply2 
	 (replicate (partition_list compare))
	 regular_pivots
	 localy_sorted) in
  parfun (List.fold_left (Sort.merge compare) []) distributed_partitions

(**)
         
let _ =
  let op x y =
    let xs = List.fold_right (fun x y -> x + y) x 0 in
    let ys = List.fold_right(fun x y -> x+y) y 0 in
    xs = ys in
  let start = Unix.gettimeofday () in
  let lsts = << generate 10_000 2_000 5 >> in
  let _ = proj <<()>> in
  let stop1 = Unix.gettimeofday () in
  let res = regular_sampling_sort_list op lsts in
  let stop2 = Unix.gettimeofday () in
  let genlist = << stop1 -. start >> in
  let exectime = << stop2 -. stop1 >> in
  let maxgen = max_of genlist Utils.nprocs in
  let maxcomp = max_of exectime Utils.nprocs in
  <<if $this$ = (Utils.nprocs-1) then Printf.printf "Max gen time: %fs\n" maxgen >>;
  <<if $this$ = (Utils.nprocs-1) then Printf.printf "Max computation time: %fs\n" maxcomp >>;
  (* << (\*List.map (fun i -> Printf.printf "%i " i) $lsts$; Printf.printf "\n";*\) *)
  (*  Printf.printf "Pid %i -> " $this$; *)
  (*  List.map (fun i -> Printf.printf "%i " i) $res$; Printf.printf "\n" >> *)
  proj <<()>>
