val systolic :
  ('a -> 'a -> 'b) ->
  ('c -> 'b Bsml.par -> 'c) -> 'a Bsml.par -> 'c -> 'c

type point = float * float * float

type atom = point * float

val read_bodies : string -> atom list

val minus_point : point -> point -> point

val length_point : point -> float

val pair_energy : atom list -> atom list -> float
