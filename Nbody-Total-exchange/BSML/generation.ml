(* nombre d'atome
   fichier *)

let rand () = string_of_float (Random.float 1000.)

let _ =
 let out=open_out Sys.argv.(2) in
 for i=1 to (int_of_string Sys.argv.(1)) do
  output_string out ((rand())^","^(rand())^","^(rand())^","^(rand())^"\n")
 done
