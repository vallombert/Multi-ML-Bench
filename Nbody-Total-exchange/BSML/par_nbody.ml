open Bsml;;

  
(*utils*)
(*********************************************************************)

let natmod i m = (m+(i mod m)) mod m

let from_to n1 n2 = 
 let rec from_to' n1 accu =  
  if n1>n2 then accu else (from_to' (n1+1) (n1::accu)) in
 List.rev (from_to' n1 [])

(* not tail recursive !!!!!!!!!!!!!! *)
let rec filtermap p f = function 
    [] -> []
  | h::t -> 
      if p h 
      then (f h)::(filtermap p f t)
      else filtermap p f t 

let is_Some = function Some _ -> true | None -> false

let noSome  = function Some d -> d | None -> assert false

let none = fun x -> None

let id x = x

let mklist x = [x]

let compose f g x = f (g x)

let map_tail f l = List.rev (List.rev_map f l)

(*********************************************************************)

let replicate x = mkpar (fun pid -> x)

let parfun f v = apply (replicate f) v

let parfun2 f v1 v2 = apply (parfun f v1) v2

let bsp_p () = bsp_p


                            
let procs () = from_to 0 (bsp_p()-1)

let this () = mkpar(fun pid->pid) 

let last () = bsp_p() - 1

let within_bounds i = (0<=i)&&(i<bsp_p())

                           
(*Comm*)


let totex vv = 
  parfun (compose noSome) (put(parfun (fun v dst->Some v) vv))

let total_exchange vec =
  parfun2 List.map (totex vec) (replicate (procs()))

let shift dec datas =
  let mkmsg = fun pid data dst -> 
    if dst=(natmod  (dec+pid) (bsp_p())) 
    then Some data
    else None in
  apply (parfun (compose noSome) (put(apply (mkpar mkmsg) datas)))
        (mkpar(fun pid->natmod (pid-dec) (bsp_p())))

let shift_right vec = shift 1 vec

let shift_left vec = shift (-1) vec

                           
let fold_direct op vec = 
  parfun (function [] -> assert false
               | h::t -> List.fold_left op h t) (total_exchange vec)

                           
(**)
(* EOF BSMLLIB *)
(**)


(** val systolic:('a -> 'a -> 'b) -> ('c -> 'b par -> 'c) -> 'a par -> 'c -> 'c 
   Generic systolic calculus *)
let systolic f op vec init =
 let rec calc n v res = 
  if n=0 then res else
   let newv=shift_right v in
    calc (n-1) newv (op res (parfun2 f vec newv))
  in calc (bsp_p()) vec init

(** Type of points and atoms *)
type point = float * float * float

type atom =  point * float

(**  val read_bodies : string -> atom list 
   Function to read from "filename" a list of atoms. One atom on each line
   which have the form x,y,z,mass where x,y,z and mass are floats *)
let read_bodies filename =
 let file=open_in filename
 and bodies = ref [] in
 try
  while true do
   match (Str.split (Str.regexp ",")  (input_line file)) with
     [x;y;z;mass] -> bodies:= (((float_of_string x),(float_of_string y),(float_of_string z)),(float_of_string mass))::!bodies
    | _ -> raise End_of_file
  done;
  !bodies
 with End_of_file -> close_in file;!bodies

(** val minus_point : point -> point -> point  *)
let minus_point (x1,y1,z1) (x2,y2,z2) = (x1-.x2,y1-.y2,z1-.z2)

(** length_point : point -> float *)
let length_point (x,y,z) = sqrt(x*.x +. y*.y+. z*.z)

(** val pair_energy : atom list -> atom list -> float 
    Calculates the interactions among the two lists of atoms *)
let pair_energy some_bodies other_bodies =
 List.fold_left (fun energy -> function (r1,m1) -> 
     energy+.(List.fold_left (fun energy -> function (r2,m2) -> 
                               let r=length_point(minus_point r2 r1) in 
		                 if r>0. then energy+.(m1*.m2)/.r else energy)
                              0. other_bodies)
                ) 0. some_bodies 

(** Main *)
let _ = 
 (* Initialiez the machine *)
 (* Each processor read its own list of atoms *)
 (* let my_bodies = mkpar (fun _ -> [((1.,2.,3.),4.);((8.,7.,6.),5.)]) *)
 let my_bodies = mkpar (fun _ -> read_bodies "nbodies.data") in
 (* On each processor calculates the interactions among them *)
 let energy=parfun2 pair_energy my_bodies my_bodies in
 (* Apply the systolic computation *) 
 let final_sys = systolic pair_energy (parfun2 (+.)) my_bodies energy in
 (* and reduce *)
 let res_final= fold_direct (+.) final_sys in ()
 (* Print the result *)
 (*ignore(mkpar (fun pid -> if pid=0 then 
                    print_string ("Total energy = "^(string_of_float res_final))
		   else ()))
  *)
