open Bsml;;

let replicate x = mkpar (fun pid -> x)

let parfun f v = apply (replicate f) v

let parfun2 f v1 v2 = apply (parfun f v1) v2

  
(** Type of points and atoms *)
type point = float * float * float

type atom =  point * float

(**  val read_bodies : string -> atom list 
   Function to read from "filename" a list of atoms. One atom on each line
   which have the form x,y,z,mass where x,y,z and mass are floats *)
let read_bodies filename =
 let file=open_in filename
 and bodies = ref [] in
 try
  while true do
   match (Str.split (Str.regexp ",")  (input_line file)) with
     [x;y;z;mass] -> bodies:= (((float_of_string x),(float_of_string y),(float_of_string z)),(float_of_string mass))::!bodies
    | _ -> raise End_of_file
  done;
  !bodies
 with End_of_file -> close_in file;!bodies

(** val minus_point : point -> point -> point  *)
let minus_point (x1,y1,z1) (x2,y2,z2) = (x1-.x2,y1-.y2,z1-.z2)

(** length_point : point -> float *)
let length_point (x,y,z) = sqrt(x*.x +. y*.y+. z*.z)

(** val pair_energy : atom list -> atom list -> float 
    Calculates the interactions among the two lists of atoms *)
let pair_energy some_bodies other_bodies =
 List.fold_left (fun energy -> function (r1,m1) -> 
     energy+.(List.fold_left (fun energy -> function (r2,m2) -> 
                               let r=length_point(minus_point r2 r1) in 
		                 if r>0. then energy+.(m1*.m2)/.r else energy)
                              0. other_bodies)
                ) 0. some_bodies 

(** Main *)
let _ = 
 (* Each processor read its own list of atoms *)
 (* let my_bodies = mkpar (fun _ -> [((1.,2.,3.),4.);((8.,7.,6.),5.)]) *)
 let my_bodies = mkpar (fun _ -> read_bodies "nbodies.data") in
 (* On each processor calculates the interactions among them *)
 let energy= parfun2 pair_energy my_bodies my_bodies in
 (* Print the result *)
 ignore(apply (mkpar (fun pid v -> if pid=0 then 
                    print_string ("Total energy = "^(string_of_float v))
		   else ())) energy)
		
