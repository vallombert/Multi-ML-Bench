open Bsml
(* let rec fusion2 l1 l2 = *)
(*   match l1,l2 with *)
(*   | _,[] -> l1 *)
(*   | [],_ -> l2 *)
(*   | hd1::tl1,hd2::tl2 -> *)
(*      if hd1 < hd2 then *)
(*        hd1::(fusion2 tl1 l2) *)
(*      else *)
(*        hd2::(fusion2 tl2 l1) *)

(* let _ = fusion2 [1;5;6;6;7;9] [2;3;4;6;9];; *)

(* let rec fusionN ll = *)
(*   List.fold_right (fun x y -> fusion2 x y) ll [] *)


(* let _ = fusionN [[1;4;7;10];[3;6;8;11];[2;5;9;10];[1;1;1;1;1]] *)


(*Arrays*)

let rec fuse2 a1 a2 =
  let l1 = Array.length a1 in
  let l2 = Array.length a2 in
  let a3 = Array.make (l1+l2) 0 in
  let cpt1 = ref 0 in
  let cpt2 = ref 0 in
  while !cpt1 < l1 && !cpt2 < l2 do
    if a1.(!cpt1) < a2.(!cpt2) then
      begin
        Array.set a3 (!cpt1 + !cpt2) a1.(!cpt1);
        incr cpt1
      end
    else
      begin
        Array.set a3 (!cpt1 + !cpt2) a2.(!cpt2);
        incr cpt2
      end
  done;
  if !cpt1 = l1 then
    while !cpt2 < l2 do
      Array.set a3 (!cpt1 + !cpt2) a2.(!cpt2);
      incr cpt2
    done
  else
    while !cpt1 < l1 do
      Array.set a3 (!cpt1 + !cpt2) a1.(!cpt1);
      incr cpt1
    done;
  a3


let fuseN (a:int array array) =
  Array.fold_right (fun x y -> fuse2 x y) a [||]
  
let split a n i =
  let slice_size = (Array.length a) / n in
  let modulo = (Array.length a) mod n in
  Array.sub a (slice_size * i) (slice_size+(if (n-1) = i then modulo else 0));;

let op_inf x y =
  compare x y
          
let bsp_fusesort arr =
  let v = mkpar (fun i -> split arr bsp_p i) in
  let sorted = << Array.sort op_inf $v$ >> in 
  let p = proj v in
  let sub_arr = Array.map (fun i -> p i) [|0;1;2;3|] in
  fuseN sub_arr;;

let generate nb =
  Array.init nb (fun i -> Random.int 1000)
  
let p=bsp_p in
    let _ = Random.init 42 in
    let values = generate (int_of_string(Sys.argv.(1))) in
    let start = Unix.gettimeofday () in
    let res = bsp_fusesort values in
    let stop1 = Unix.gettimeofday () in
    let exectime = << (stop1 -. start) >> in
    let pr = proj exectime in
    let res = Array.init (p) (fun i -> pr i) in
    let max = Array.fold_left (max) (Array.get res 0) res in
    (* Printf.printf "Execution time: %fs\n%!" (stop1 -. start); *)
    <<if $this$ = (p-1) then Printf.printf "Total Execution time: %fs\n" max)>>
