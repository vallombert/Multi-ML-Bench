type 'a flow = 'a list

let length = List.length

let of_list l = l

let to_list s = s

let map = List.map

let mapidx g l = 
 let rec tmp n accu =  
   function [] -> List.rev accu 
      | hd::tl -> tmp (n+1) ((g n hd)::accu) tl 
 in tmp 1 [] l

let zip oplus l1 l2 =
 let rec tmp accu l1 l2 =
  match l1, l2 with
    [], [] -> List.rev accu
  | hd1::tl1, hd2::tl2 -> tmp ((oplus hd1 hd2)::accu) tl1 tl2
  | _, _ -> invalid_arg "zip"
 in tmp [] l1 l2

let reduce = List.fold_left

(* tools *)
let rec tmp_scan f accu accu_list l =
  match l with
    [] -> List.rev accu_list
  | hd::tl -> let new_e=(f accu hd) 
             in tmp_scan f new_e (new_e::accu_list) tl

let scan oplus e l = tmp_scan oplus e [] l

(* tools *)
let take_n l n = 
 let rec tmp accu l n = 
  if n=0 then (List.rev accu,l) else 
   match l with
      [] -> invalid_arg "take_n in dh"
    | hd::tl -> tmp (hd::accu) tl (n-1) 
 in tmp [] l n

(* tools *)
let combine u v op =
let rec tmp u v accu =
 match u,v with
  | [], [] -> accu
  | hd1::tl1, hd2::tl2 ->  tmp tl1 tl2 ((op hd1 hd2)::accu)
  | _,_ -> invalid_arg "combine in dh"
in tmp u v []

let dh oplus omult x = 
 let rec tmp n l = 
   if n<=1 then l else
    let u,v=take_n l (n/2) in
    let u,v=tmp (n/2) u, tmp (n/2) v in
     List.rev_append (combine u v oplus) (List.rev (combine u v omult))
 in tmp (List.length x) x     
    
let repl x n = Array.to_list (Array.make n x)
