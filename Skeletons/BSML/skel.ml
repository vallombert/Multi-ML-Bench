open Bsmllib
open Bsmlutils
open Bsmlbase

type 'a flow = 'a array par * int

let length (f,n) = n

let of_list l = 
  let p=bsp_p() in
  let al = Array.of_list l in
  let len=Array.length al in 
  let nb = len mod p in
   (mkpar (fun pid -> if pid<nb 
                       then Array.sub al (pid*((len/p)+1)) (len/p+1)
                      else Array.sub al ((pid*(len/p))+nb) (len/p)),len)

let to_list (fl,_) = 
 Array.to_list (Array.concat (Bsmlcomm.replicate_total_exchange fl))

let map f (fl,n) = (parfun (Array.map f) fl,n)

let mapidx f (fl,n) = (parfun (Array.mapi f) fl,n)

(* unsafe *)
let array_zip n op a1 a2 = 
 if n=0 then [||] else 
  let res=Array.create n (op a1.(0) a2.(0)) in
   for i = 1 to pred n do
     Array.unsafe_set res i (op (Array.unsafe_get a1 i) (Array.unsafe_get a2 i))
   done;
   res

let zip op (fl1,n1) (fl2,n2) = 
 if n1<>n2 
  then invalid_arg "zip"
 else  (parfun2 (array_zip n1 op) fl1 fl2,n1)

let repl a n =
  if n<0 then invalid_arg "repl" 
  else
   let p=bsp_p() in
   let nb = n mod p in
    (mkpar (fun pid -> if pid<nb 
                        then Array.create (n/p+1) a
                       else Array.create (n/p) a),n)

let super_mix m (v1,v2) = 
 let f pid v1 v2 = if pid<=m then v1 else v2 in 
  apply2 (mkpar f) v1 v2

let inbounds first last n = (first<=n)&&(n<=last)

let scan_super op e vec =
 let rec scan' fst lst op vec =
  if fst>=lst then vec 
   else 
    let mid = (fst+lst)/2 in
    let vec'= super_mix mid (super (fun()->scan' fst mid op vec)
			           (fun()->scan'(mid+1) lst op vec)) in
    let msg vec = apply (mkpar(fun i v->
      if i=mid 
       then (fun dst->if inbounds (mid+1) lst dst then Some v else None)
      else (fun dst-> None))) vec
    and parop = parfun2(fun x y->match x with None->y|Some v->op v y) in
    parop (apply(put(msg vec'))(mkpar(fun i->mid))) vec' in
  applyat 0 (fun _ ->e) (fun x->x) (scan' 0 (bsp_p()-1) op vec)


let replicated_fold op e replicate_bcast scan vec =
 replicate_bcast (bsp_p()-1) (scan op e vec)

let replicated_fold_super op vl e = 
 replicated_fold op e Bsmlcomm.replicate_bcast_direct scan_super vl

let replicate_fold_array_super op e va = Bsmlcomm.fold_array replicated_fold_super op e va

let reduce op e (fl,n) = replicate_fold_array_super op fl e

let array_seq_scan_last op e arr = 
 let len=Array.length arr in
 let arr'=Array.init len (fun i->arr.(i)) in
  for i=1 to (len-1) do 
   Array.set arr' i (op (arr'.(i-1)) (arr'.(i))) 
  done;
  (arr'.(len-1),arr')

let array_rev_map f arr = 
 let len=(Array.length arr) in
  Array.init len (fun i-> f arr.(len-i))

let scan_wide scan seq_scan_last map op e vl =
 let local_scan=parfun (seq_scan_last op e) vl in
 let last_elements=parfun fst local_scan in
 let values_to_add=(scan op e last_elements) in
 let pop=applyat 0 (fun _ y->y) op in
  parfun2 map (pop values_to_add) (parfun snd local_scan)

let scan_wide_array scan op e va = 
 scan_wide scan array_seq_scan_last array_rev_map op e va

let scan_array_super op e va = scan_wide_array scan_super op e va

let scan op e (fl,n) = (scan_array_super op e fl, n)



(* dh *)
(* suppose p=2^n >= 2^l*)

(* local unsafe*)
let local_dh oplus omult t =
 let rec tmp n1 n2 n =
  if n=1 then () else
   let n'=n/2 in
   let n1'=n1+n' and n2'=n1+n'-1 in
   begin
    tmp n1 n2' n';
    tmp n1' n2 n';
    let memo=Array.sub t n1 (n'+1) in
     for i=n1 to n2' do
      Array.unsafe_set t i (oplus (Array.unsafe_get t i) (Array.unsafe_get t (i+n')));
      Array.unsafe_set t (i+n') (omult (Array.unsafe_get memo (i-n1)) (Array.unsafe_get t (i+n')))
     done
   end
  in
   tmp 0 ((Array.length t)-1) (Array.length t);t;;

let array_map2 f a b =
  let l = Array.length a in
  if l = 0 then [||] else begin
    let r = Array.create l (f (Array.unsafe_get a 0) (Array.unsafe_get b 0)) in
    for i = 1 to l - 1 do
      Array.unsafe_set r i (f (Array.unsafe_get a i) (Array.unsafe_get b i))
    done;
    r
  end

let dh oplus omult (fl,len) = 
 let rec tmp n1 n2 n vec =
  if n=1 then vec else
   let n'=n/2 in
   let n1'=n1+n' and n2'=n1+n'-1 in
   let vec'= super_mix (n1'-1) (super (fun () -> tmp n1 n2' n' vec) 
                                      (fun () -> tmp n1' n2 n' vec)) in
   let msg=mkpar (fun pid v -> 
                    if pid<n1' 
		     then (fun dst -> if dst=(pid+n') then Some v else None)
		    else (fun dst -> if dst=(pid-n') then Some v else None))
 in
   let send=put (Bsmllib.apply msg vec') in
   let rcv = mkpar (fun pid f a -> if pid<n1' 
                            then match (f (pid+n')) with
			            Some b -> array_map2 oplus a b
				  | None -> a
		             else match (f (pid-n')) with
			            Some b -> array_map2 omult b a
				  | None -> a) in
   apply2 rcv send vec'
  in (tmp 0 (bsp_p()-1) (bsp_p()) (parfun (local_dh oplus omult) 
                                         (parfun Array.copy fl)),len)


let apply f a = f a

(* 

version 0.5

open Bsmllib
open Bsmlutils

type 'a flow = 'a array par * int

let length (f,n) = n

let of_list l = 
  let p=bsp_p() in
  let al = Array.of_list l in
  let len=Array.length al in 
  let nb = len mod p in
   (mkpar (fun pid -> if pid<nb 
                       then Array.sub al (pid*((len/p)+1)) (len/p+1)
                      else Array.sub al ((pid*(len/p))+nb) (len/p)),len)

let to_list (fl,_) = 
 Array.to_list (Array.concat (Bsmlcomm.replicate_total_exchange fl))

let map f (fl,n) = (parfun (Array.map f) fl,n)

let mapidx f (fl,n) = (parfun (Array.mapi f) fl,n)

let array_zip n op a1 a2 = 
 if n=0 then [||] else 
  let res=Array.create n (op a1.(0) a2.(0)) in
   for i = 1 to pred n do
     Array.unsafe_set res i (op (Array.unsafe_get a1 i) (Array.unsafe_get a2 i))
   done;
   res

let zip op (fl1,n1) (fl2,n2) = 
 if n1<>n2 
  then invalid_arg "zip"
 else  (parfun2 (array_zip n1 op) fl1 fl2,n1)

let apply f a = f a

let repl a n =
  if n<0 then invalid_arg "repl" 
  else
   let p=bsp_p() in
   let nb = n mod p in
    (mkpar (fun pid -> if pid<nb 
                        then Array.create (n/p+1) a
                       else Array.create (n/p) a),n)

let reduce op e (fl,n) = Bsmlcomm.replicate_fold_array_super op e fl

let scan op e (fl,n) = (Bsmlcomm.scan_array_super op e fl, n)

let local_dh oplus omult t =
 let rec tmp n1 n2 n =
  if n=1 then () else
   let n'=n/2 in
   let n1'=n1+n' and n2'=n1+n'-1 in
   begin
    tmp n1 n2' n';
    tmp n1' n2 n';
    let memo=Array.sub t n1 (n'+1) in
     for i=n1 to n2' do
      Array.unsafe_set t i (oplus (Array.unsafe_get t i) (Array.unsafe_get t (i+n')));
      Array.unsafe_set t (i+n') (omult (Array.unsafe_get memo (i-n1)) (Array.unsafe_get t (i+n')))
     done
   end
  in
   tmp 0 ((Array.length t)-1) (Array.length t);t;;

let array_map2 f a b =
  let l = Array.length a in
  if l = 0 then [||] else begin
    let r = Array.create l (f (Array.unsafe_get a 0) (Array.unsafe_get b 0)) in
    for i = 1 to l - 1 do
      Array.unsafe_set r i (f (Array.unsafe_get a i) (Array.unsafe_get b i))
    done;
    r
  end

let dh oplus omult (fl,len) = 
 let rec tmp n1 n2 n vec =
  if n=1 then vec else
   let n'=n/2 in
   let n1'=n1+n' and n2'=n1+n'-1 in
   let vec'= super_mix (n1'-1) (super (fun () -> tmp n1 n2' n' vec) 
                                      (fun () -> tmp n1' n2 n' vec)) in
   let msg=mkpar (fun pid v -> 
                    if pid<n1' 
		     then (fun dst -> if dst=(pid+n') then Some v else None)
		    else (fun dst -> if dst=(pid-n') then Some v else None))
 in
   let send=put (Bsmllib.apply msg vec') in
   let rcv = mkpar (fun pid f a -> if pid<n1' 
                            then match (f (pid+n')) with
			            Some b -> array_map2 oplus a b
				  | None -> a
		             else match (f (pid-n')) with
			            Some b -> array_map2 omult b a
				  | None -> a) in
   apply2 rcv send vec'
  in (tmp 0 (bsp_p()-1) (bsp_p()) (parfun (local_dh oplus omult) 
                                         (parfun Array.copy fl)),len)

*)
