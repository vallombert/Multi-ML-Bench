open Bsml

(***DH***)
       
let apply2 f v1 v2 = apply (apply f v1) v2

let super_mix m (v1,v2) = 
  let f pid v1 v2 = if pid<=m then v1 else v2 in 
  apply2 (mkpar f) v1 v2

let super f g = (f(),g())

let replicate x = mkpar (fun pid->x)

let parfun f v = apply (replicate f) v

let array_map2 f a b =
  let l = Array.length a in
  if l = 0 then [||] else begin
      let r = Array.make l (f (Array.unsafe_get a 0) (Array.unsafe_get b 0)) in
      for i = 1 to l - 1 do
        Array.unsafe_set r i (f (Array.unsafe_get a i) (Array.unsafe_get b i))
      done;
      r
    end

(* local unsafe*)
let local_dh oplus omult t=
  let rec tmp n1 n2 n =
    if n=1 then () else
      let n'=n/2 in
      let n1'=n1+n' and n2'=n1+n'-1 in
      begin
        tmp n1 n2' n';
        tmp n1' n2 n';
        let memo=Array.sub t n1 (n'+1) in
        for i=n1 to n2' do
          Array.unsafe_set t i (oplus (Array.unsafe_get t i) (Array.unsafe_get t (i+n')));
          Array.unsafe_set t (i+n') (omult (Array.unsafe_get memo (i-n1)) (Array.unsafe_get t (i+n')))
        done
      end
  in
  tmp 0 ((Array.length t)-1) (Array.length t);t;;

                            
let dh oplus omult (fl,len) =
  let rec tmp n1 n2 n vec =
    (* << Array.iter (fun x -> Printf.printf "%i " x) $vec$ >>; *)
    (* Printf.printf "\n"; *)
    if n=1 then vec else
      let n'=n/2 in
      let n1'=n1+n' and n2'=n1+n'-1 in
      let vec'= super_mix (n1'-1) (super (fun () -> tmp n1 n2' n' vec)
                                         (fun () -> tmp n1' n2 n' vec)) in
      let msg=mkpar (fun pid v ->
                  if pid<n1'
		  then (fun dst -> if dst=(pid+n') then Some v else None)
		  else (fun dst -> if dst=(pid-n') then Some v else None))
      in
      let send=put (apply msg vec') in
      let rcv = mkpar (fun pid f a -> if pid<n1'
                                      then match (f (pid+n')) with
			                     Some b -> array_map2 oplus a b
				           | None -> a
		                      else match (f (pid-n')) with
			                     Some b -> array_map2 omult b a
				           | None -> a) in
      apply2 rcv send vec'
  in (tmp 0 (bsp_p-1) (bsp_p) (parfun (local_dh oplus omult)
                                          (parfun Array.copy fl)),len);;

let map f (fl,n) = (parfun (Array.map f) fl,n)

let mapidx f (fl,n) = (parfun (Array.mapi f) fl,n)

                        
let of_list l = 
  let p=bsp_p in
  let al = Array.of_list l in
  let len=Array.length al in 
  let nb = len mod p in
  (mkpar (fun pid -> if pid<nb 
                     then Array.sub al (pid*((len/p)+1)) (len/p+1)
                     else Array.sub al ((pid*(len/p))+nb) (len/p)),len)


(***Testing***)
let rec from_to n1 n2 = if n1>n2 then [] else n1::(from_to (n1+1) n2)
let procs = from_to 0 (bsp_p-1)                                              

let split a n i =
  let slice_size = (Array.length a) / n in
  let modulo = (Array.length a) mod n in
  Array.sub a (slice_size * i) (slice_size+(if (n-1) = i then modulo else 0));;
  
let generate nb = Array.init nb (fun x -> x+1)
                             

let p=bsp_p in
let vals = mkpar (fun i -> split (generate (int_of_string Sys.argv.(1))) p i) in
    let start = Unix.gettimeofday () in
    let res = fst (dh (+) ( * ) (vals,0)) in
    let stop1 = Unix.gettimeofday () in
    (* << Array.iter (fun i -> Printf.printf "%i" i) $res$ >>; *)
    let exectime = << (stop1 -. start) >> in
    let pr = proj exectime in
    let exec = Array.init (p) (fun i -> pr i) in
    let max = Array.fold_left (max) (Array.get exec 0) exec in
    (* Printf.printf "Execution time: %fs\n%!" (stop1 -. start); *)
    <<if $this$ = (p-1) then Printf.printf "Total Execution time: %fs\n" max)>>;
    (* << Printf.printf "Output list size %i\n" (Array.length $res$) >>; *)
    (* let p = proj res in *)
    (* let total = Array.concat (List.mapi (fun i x-> p i) procs) in *)
    (* << if $this$ = 0 then ignore (Array.map (fun x -> Printf.printf "%i " x) total) >> *)
