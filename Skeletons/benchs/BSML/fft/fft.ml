open Bsml

(***DH***)
       
let apply2 f v1 v2 = apply (apply f v1) v2

let super_mix m (v1,v2) = 
  let f pid v1 v2 = if pid<=m then v1 else v2 in 
  apply2 (mkpar f) v1 v2

let super f g = (f(),g())

let replicate x = mkpar (fun pid->x)

let parfun f v = apply (replicate f) v

let array_map2 f a b =
  let l = Array.length a in
  if l = 0 then [||] else begin
      let r = Array.make l (f (Array.unsafe_get a 0) (Array.unsafe_get b 0)) in
      for i = 1 to l - 1 do
        Array.unsafe_set r i (f (Array.unsafe_get a i) (Array.unsafe_get b i))
      done;
      r
    end

(* local unsafe*)
let local_dh oplus omult t =
  let rec tmp n1 n2 n =
    if n=1 then () else
      let n'=n/2 in
      let n1'=n1+n' and n2'=n1+n'-1 in
      begin
        tmp n1 n2' n';
        tmp n1' n2 n';
        let memo=Array.sub t n1 (n'+1) in
        for i=n1 to n2' do
          Array.unsafe_set t i (oplus (Array.unsafe_get t i) (Array.unsafe_get t (i+n')));
          Array.unsafe_set t (i+n') (omult (Array.unsafe_get memo (i-n1)) (Array.unsafe_get t (i+n')))
        done
      end
  in
  tmp 0 ((Array.length t)-1) (Array.length t);t;;

                            
let dh oplus omult (fl,len) =
  let rec tmp n1 n2 n vec =
    if n=1 then vec else
      let n'=n/2 in
      let n1'=n1+n' and n2'=n1+n'-1 in
      let vec'= super_mix (n1'-1) (super (fun () -> tmp n1 n2' n' vec)
                                         (fun () -> tmp n1' n2 n' vec)) in
      let msg=mkpar (fun pid v ->
                  if pid<n1'
		  then (fun dst -> if dst=(pid+n') then Some v else None)
		  else (fun dst -> if dst=(pid-n') then Some v else None))
      in
      let send=put (apply msg vec') in
      let rcv = mkpar (fun pid f a -> if pid<n1'
                                      then match (f (pid+n')) with
			                     Some b -> array_map2 oplus a b
				           | None -> a
		                      else match (f (pid-n')) with
			                     Some b -> array_map2 omult b a
				           | None -> a) in
      apply2 rcv send vec'
  in (tmp 0 (bsp_p-1) (bsp_p) (parfun (local_dh oplus omult)
                                          (parfun Array.copy fl)),len);;

                        
let of_list l = 
  let p=bsp_p in
  let al = Array.of_list l in
  let len=Array.length al in 
  let nb = len mod p in
   (mkpar (fun pid -> if pid<nb 
                       then Array.sub al (pid*((len/p)+1)) (len/p+1)
                      else Array.sub al ((pid*(len/p))+nb) (len/p)),len)
let mapidx f (fl,n) = (parfun (Array.mapi f) fl,n)
(***FFT***)

(* tools *)
let int_to_complex i = {Complex.re=(float)i;im=0.}
let float_to_complex f = {Complex.re=f;im=0.}

let w n = 
 Complex.exp  (Complex.mul 
                 (float_to_complex (2.*.3.14/.(float)n))
		 (Complex.sqrt Complex.i))

(* fft :  Complex.t list -> Complex.t list *)
let fft l =
 let opluschap i n a b = 
 Complex.add a (Complex.mul (Complex.pow (w n) (int_to_complex i)) b)
 and omultchap i n a b = 
 Complex.sub a (Complex.mul (Complex.pow (w n) (int_to_complex i)) b) in
 let triple i x = (x,i,1)
 and oplus (x1,i1,n1) (x2,i2,n2) = (opluschap i1 n1 x1 x2, i1, 2*n1)
 and omult (x1,i1,n1) (x2,i2,n2) = (omultchap i1 n1 x1 x2, i1, 2*n1)
 and pi1 (a,_,_) = a in
(* in map pi1 (dh oplus omult (mapidx triple l)) *)
let vals = mapidx triple l in
    dh oplus omult vals

let f = ref 0.0
let getf = let v = !f in let _ = f := !f +. 1.0 in v
let random_complex () = {Complex.re=Random.float 100_000.;im=Random.float 100_000.}

let generate_list n =
 let rec tmp n accu = 
  if n=0 then accu else
    tmp (n-1) (random_complex()::accu) in
 tmp (2 lsl n) []

  
(***Testing***)

(* let init size np = *)
(*   mkpar (fun i -> Array.init (size/4) (fun j -> j + (i*(size/4)))) *)
  
(* let vals = init (16*16*16*16*16*16) 4 in *)
(*     let res = fst (dh (+) ( * ) (vals,4)) in *)
(*     << Printf.printf "%i\n" $res$.(0) >>;; *)
     

let p=bsp_p in
    let n=int_of_string (Sys.argv.(1)) in
    let l=of_list (generate_list n) in
    let start = Unix.gettimeofday () in
    let res =
      fst (fft l)
    in
    let stop1 = Unix.gettimeofday () in
    let exectime = << (stop1 -. start) >> in
    let pr = proj exectime in
    let restime = Array.init (p) (fun i -> pr i) in
    let max = Array.fold_left (max) (Array.get restime 0) restime in
    (* Printf.printf "Execution time: %fs\n%!" (stop1 -. start); *)
    <<if $this$ = (0) then Printf.printf "Total Execution time: %fs\n" max)>> 
    (*;let cplx = << ((fun (x,_,_) -> x) (Array.get $res$ 0))>> in
    << Printf.printf "%f \n" (let v = $cplx$ in v.Complex.im) >>*)
    (* ;<<if $this$ = (p-1) then       ignore (Array.map (fun (c,i1,i2) -> Printf.printf "(%f,%f)(%i,%i)\n" (c.Complex.re) (c.im) i1 i2) ($res$)); Printf.printf "Res length :%i\n" (Array.length $res$) >> *)
