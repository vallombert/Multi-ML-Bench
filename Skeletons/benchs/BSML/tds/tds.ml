open Bsml


(*Skels*)

let super f g = (f(),g())
       
let replicate x = mkpar (fun pid->x)

let parfun f v = apply (replicate f) v
       
let map f (fl,n) = (parfun (Array.map f) fl,n)

let apply2 f v1 v2 = apply (apply f v1) v2

let super_mix m (v1,v2) = 
  let f pid v1 v2 = if pid<=m then v1 else v2 in 
  apply2 (mkpar f) v1 v2

let array_map2 f a b =
  let l = Array.length a in
  if l = 0 then [||] else begin
      let r = Array.make l (f (Array.unsafe_get a 0) (Array.unsafe_get b 0)) in
      for i = 1 to l - 1 do
        Array.unsafe_set r i (f (Array.unsafe_get a i) (Array.unsafe_get b i))
      done;
      r
    end
         
let local_dh oplus omult t =
  let rec tmp n1 n2 n =
    if n=1 then () else
      let n'=n/2 in
      let n1'=n1+n' and n2'=n1+n'-1 in
      begin
        tmp n1 n2' n';
        tmp n1' n2 n';
        let memo=Array.sub t n1 (n'+1) in
        for i=n1 to n2' do
          Array.unsafe_set t i (oplus (Array.unsafe_get t i) (Array.unsafe_get t (i+n')));
          Array.unsafe_set t (i+n') (omult (Array.unsafe_get memo (i-n1)) (Array.unsafe_get t (i+n')))
        done
      end
  in
  tmp 0 ((Array.length t)-1) (Array.length t);t;;

                            
let dh oplus omult (fl,len) =
  let rec tmp n1 n2 n vec =
    if n=1 then vec else
      let n'=n/2 in
      let n1'=n1+n' and n2'=n1+n'-1 in
      let vec'= super_mix (n1'-1) (super (fun () -> tmp n1 n2' n' vec)
                                         (fun () -> tmp n1' n2 n' vec)) in
      let msg=mkpar (fun pid v ->
                  if pid<n1'
		  then (fun dst -> if dst=(pid+n') then Some v else None)
		  else (fun dst -> if dst=(pid-n') then Some v else None))
      in
      let send=put (apply msg vec') in
      let rcv = mkpar (fun pid f a -> if pid<n1'
                                      then match (f (pid+n')) with
			                     Some b -> array_map2 oplus a b
				           | None -> a
		                      else match (f (pid-n')) with
			                     Some b -> array_map2 omult b a
				           | None -> a) in
      apply2 rcv send vec'
  in (tmp 0 (bsp_p-1) (bsp_p) (parfun (local_dh oplus omult)
                                          (parfun Array.copy fl)),len);;                     

let of_list l = 
  let p=bsp_p in
  let al = Array.of_list l in
  let len=Array.length al in 
  let nb = len mod p in
   (mkpar (fun pid -> if pid<nb 
                       then Array.sub al (pid*((len/p)+1)) (len/p+1)
                      else Array.sub al ((pid*(len/p))+nb) (len/p)),len)












  
let (/) a b = if b=0 then a else a/b

let un ((a1,a2,a3,a4):int*int*int*int) (b1,b2,b3,b4) = 
 try (a1,a3-(a2/b1)*b2,b3*(-a2/b1),a4-(a2/b1)*b4) with e -> print_string "coucou un\n";raise e

let deux ((a1,a2,a3,a4):int*int*int*int) (b1,b2,b3,b4) = 
 try (a1-(a3/b2)*b1,a2,(-a3/b2)*b3,a4-(a3/b2)*b4) with e -> print_string "coucou deux\n";raise e

let trois ((a1,a2,a3,a4):int*int*int*int) (b1,b2,b3,b4) = 
 try (a1,a2-(b1*a3/b2),(-b3*a3/b2),a4-(a3/b2)*b4) with e -> print_string "coucou trois\n";raise e

let quatre ((a1,a2,a3,a4):int*int*int*int) (b1,b2,b3,b4) = 
 try (a1,-(a2/b1)*b2,a3-(b3*a2/b1),a4-(b4*a2)/b1) with e -> print_string "coucou quatre\n";raise e

(* tds : (int * int * int * int) list -> (int * int * int * int) list *)
let tds a =
 let oplus (a1,f1,l1) (a2,f2,l2) 
   = ((deux a1 (un l1 f2)), (deux f1 (un l1 f2)),(quatre (trois l1 f2) l2))
 and omult (a1,f1,l1) (a2,f2,l2) 
   = ((quatre (trois l1 f2) a2), (deux f1 (un l1 f2)), (quatre (trois l1 f2) l2))
 and triple a = (a,a,a)
 and pi1 (a,_,_) = a
 in 
  map pi1 (dh oplus omult (map triple a))

let generate_list n =
 let rec tmp n accu = 
  if n=0 then ((0,Random.int 100000,0,Random.int 100000)::accu) else
    tmp (n-1) ((Random.int 100000,Random.int 100000,Random.int 100000,Random.int 100000)::accu) in
 tmp ((2 lsl n)-2) [(0,Random.int 100000,0,Random.int 100000)]


(*Usage: mpirun -n XX ./tds n*)
let _ =
  let p = bsp_p in
  let n=(int_of_string (Sys.argv).(1)) in
  let l=of_list (generate_list n) in
  let start = Unix.gettimeofday () in
  tds l;
  let stop1 = Unix.gettimeofday () in
  let exectime = << (stop1 -. start) >> in
  let pr = proj exectime in
  let restime = Array.init (p) (fun i -> pr i) in
  let max = Array.fold_left (max) (Array.get restime 0) restime in
  <<if $this$ = (p-1) then Printf.printf "Total Execution time: %fs\n" max)>>
