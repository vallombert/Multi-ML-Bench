let height = 10
let width = 10

let mat = Array.init (height*width) (fun i -> i)


let split_mat mat width height nb_slices i =
  let slice_size = height / nb_slices in
  let start = i * slice_size * width in
  let length = (if i = nb_slices -1 then
                  slice_size + (height - slice_size * nb_slices)
                else slice_size) * width
  in Array.sub mat start length;;

let from_to n1 n2 = 
  let rec from_to' n1 accu =  
    if n1>n2 then accu else (from_to' (n1+1) (n1::accu)) in
  List.rev (from_to' n1 [])

let nb_children () = 4
           
let mat_combine_slices f =
  Array.concat (List.map f (from_to 0 (nb_children()-1)))

mat_combine_slices (function | 0 -> [|0;1;2;3|] | 1 -> [|4;5;6;7|] | 2 -> [|8;9;10;11|] | 3 -> [|12;13;14;15|])
