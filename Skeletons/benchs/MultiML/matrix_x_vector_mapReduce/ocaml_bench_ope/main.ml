let display_matrix mat h w =
  for i = 0 to h-1 do
    for j = 0 to w-1 do
      Printf.printf "%i " (Array.get mat (i*w+j));
    done;
    Printf.printf "\n";
  done
    
(* mat(h*w) vec(w*1) -> out(h*1 )*)
let mat_x_vec mat h w vec out =
  for i = 0 to h-1 do
    for j = 0 to w-1 do
      Array.set out i (Array.get out i + (Array.get mat (i*w+j)) * (Array.get vec j));
    done;
  done;
  out


    
(* ocamlfind ocamlopt -o main -package core -package core_bench -linkpkg -thread main.ml*)
open Core.Std
open Core_bench.Std

    
let _ =
  let height = 30_000 in
  let width = 15_000 in
  let mat = Array.init (height*width) (fun i -> i) in
  let vec = Array.init (width) (fun i -> i) in
  let out = Array.create height 0 in
  (* display_matrix mat height width; *)
  (* display_matrix vec width 1; *)
  (* let start = Unix.gettimeofday () in *)
  (* for i = 0 to 2 do *)
  (*   let _ = mat_x_vec mat height width vec out in () *)
  (* done; *)
  (* let stop = Unix.gettimeofday () in *)
  (* let _ = Printf.printf "Total Execution time: %fs\n" ((stop-.start)/.2.) *)
  let _ = Command.run
            (Bench.make_command[
                 Bench.Test.create ~name:"Set"
                                   (fun () -> ignore (Array.set out 1 1));
                 Bench.Test.create ~name:"Get"
                                   (fun () -> ignore (Array.get out 1));
                 Bench.Test.create ~name:"Add"
                                   (fun () -> ignore (1+1));
                 Bench.Test.create ~name:"Multi"
                                   (fun () -> ignore (1*1));
                 (* Bench.Test.create ~name:"Multi" *)
                 (*                   (fun () -> ignore (mat_x_vec mat height width vec out)); *)
            ])
  in ()
       (* display_matrix out height 1 *)
