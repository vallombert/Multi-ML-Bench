
let display_matrix mat h w =
  for i = 0 to h-1 do
    for j = 0 to w-1 do
      Printf.printf "%i " (Array.get mat (i*w+j));
    done;
    Printf.printf "\n";
  done
    
(* mat(h*w) vec(w*1) -> out(h*1 )*)
let mat_x_vec mat h w vec =
  let out = Array.make h 0 in
  for i = 0 to h-1 do
    for j = 0 to w-1 do
      Array.set out i (Array.get out i + (Array.get mat (i*w+j)) * (Array.get vec j));
    done;
  done;
  out

    
(* (\* mat(h*w) vec(w*1) -> out(h*1 )*\) *)
(* let mat_x_vec_read mat h w vec = *)
(*   let out = Array.make h 0 in *)
(*   for i = 0 to h-1 do *)
(*     for j = 0 to w-1 do *)
(*       let a = Array.get out i in *)
(*       let b = (Array.get mat (i*w+j)) in *)
(*       let c = (Array.get vec j) in () *)
(*     done; *)
(*   done; *)
(*   out *)

(* (\* mat(h*w) vec(w*1) -> out(h*1 )*\) *)
(* let mat_x_vec_read_mult mat h w vec = *)
(*   let out = Array.make h 0 in *)
(*   for i = 0 to h-1 do *)
(*     for j = 0 to w-1 do *)
(*       let a = Array.get out i in *)
(*       let b = (Array.get mat (i*w+j)) in *)
(*       let c = (Array.get vec j) in *)
(*       let d = b * c in () *)
(*     done; *)
(*   done; *)
(*   out *)


(* (\* mat(h*w) vec(w*1) -> out(h*1 )*\) *)
(* let mat_x_vec_read_mult_add mat h w vec = *)
(*   let out = Array.make h 0 in *)
(*   for i = 0 to h-1 do *)
(*     for j = 0 to w-1 do *)
(*       let a = Array.get out i in *)
(*       let b = (Array.get mat (i*w+j)) in *)
(*       let c = (Array.get vec j) in *)
(*       let d = a + b * c in () *)
(*     done; *)
(*   done; *)
(*   out *)

    
(*Seq code*)
    
    
let _ =
  let height = 1_000 in
  let width = 1_000 in
  let mat = Array.init (height*width) (fun i -> i) in
  let vec = Array.init (width) (fun i -> i) in
  (* display_matrix mat height width; *)
  (* display_matrix vec width 1; *)
  let start = Unix.gettimeofday () in
  let nb_it = 0 in
  for i = 0 to nb_it do
    let out = mat_x_vec mat height width vec in
    ()
      (*;display_matrix out height 1*)
  done;
  let stop = Unix.gettimeofday () in
  Printf.printf "Total computation time: %fs\n" ((stop-.start)/.(float_of_int (nb_it +1)));

