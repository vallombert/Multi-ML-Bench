(*   ---- UTILS ---- *)
(*Get a sublist from a list*)
let rec sublist b e l = 
  match l with
    [] -> failwith "sublist"
  | h :: t -> 
     let tail = if e=0 then [] else sublist (b-1) (e-1) t in
     if b>0 then tail else h :: tail
;;

(*Returns the i^st slice of a list split in n elements*)
let split a n i =
  let slice_size = (List.length a) / n in
  let modulo = (List.length a) mod n in
  sublist (slice_size * i) (slice_size * i+slice_size+(if (n-1) = i then modulo else 0)-1) a;;
  

(*Distribute a list on the leaves of a tree*)
let multi tree distributed_tree_list l =
  where node = 
    let slices = mkpar (fun i -> split l (nb_children ()) i) in 
    let rc = << distributed_tree_list $slices$ >> in
    finally (rc,[])
      where leaf =
      l;;

(*   ---- MAP ----   *)

(* map (f:'a -> 'b) (tdl:'a list tree)*)
let m_map f tdl =
  let multi tree map tdl =
    where node =
      let rc = << map tdl >> in
      finally(rc,[])
      where leaf =
        List.map f (at tdl)
  in map tdl;;


(*   ---- REDUCE ----   *)

let to_list vec =
  let p = proj vec in
  List.map p (my_children())
  
  
(*reduce (op:'a -> 'a -> 'a) (e:'a) (tdl : 'a list tree)*)
let m_reduce op e tdl =
  let multi reduce tdl =
    where node =
      let rc = << reduce tdl >> in
      let sub_vals = to_list rc in
      List.fold_left op e sub_vals
        where leaf =
        List.fold_left op e (at tdl)
  in reduce tdl;;

  
(*   ---- MAP/REDUCE ----   *)
let m_map_reduce op_map op_red e tdl =
  let multi map_reduce tdl =
    where node =
      let rc = << map_reduce tdl >> in
      let sub_vals = to_list rc in
      List.fold_left op_red e sub_vals
    where leaf =
      let res_map = List.map op_map (at tdl) in
      List.fold_left op_red e res_map
  in map_reduce tdl;;
  
  
(*   ---- MAIN ----   *) 
let _ =
  let op_map = (fun x -> x*10) in
  let op_red = (fun x y -> x + y) in
  let neutral = 0 in
  (*DISTRIBUTE VALUES*)
  let tdl = distributed_tree_list [1;2;3;4;5;6;7;8] in
  let _ = Printf.printf "%i -> [" (pid());
          (List.map (fun x -> Printf.printf "%i " x) (at tdl));
          Printf.printf "]\n" in
  (*MAP*)
  let res_map = m_map op_map tdl in
  let _ = Printf.printf "%i -> [" (pid());
          (List.map (fun x -> Printf.printf "%i " x) (at res_map));
          Printf.printf "]\n" in
  (*REDUCE*)
  let res_red = m_reduce op_red neutral res_map in
  let _ = Printf.printf "Reduce res = %i\n" res_red in  
  (*MAPREDUCE*)
  let res_map_red = m_map_reduce op_map op_red neutral tdl in
  Printf.printf "Map/Reduce res = %i\n" res_red 
  
