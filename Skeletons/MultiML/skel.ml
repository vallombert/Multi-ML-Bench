(*The data is distributed on each component of the architecture*)
type 'a treeflow = 'a array tree * int

(*The data is available on the leaf only*)
type 'a leafflow = 'a array option tre * int                         

(* ------------- *)
(* ---- MAP ---- *)
(* ------------- *)
                                           
let (map_treeflow : ('a -> 'b) -> 'a treeflow -> 'b treeflow) f (t,n) =
  let multi tree map () =
    where node =
      let rc = << map () >> in
      let res = Array.map f (at_b t) in
      finally rc res
    where leaf =
      Array.map f (at_s t)
  in (map (),n)
  

let (map_leafflow : ('a -> 'b) -> 'a leafflow -> 'b leafflow) f (t,n) =
  let multi tree map () =
    where node =
     let rc = << map () >> in
     finally (rc,None)
    where leaf =
       Some (Array.map f (at_s t))
  in (map (),n)

         
(* ------------- *)
(* ---- ZIP ---- *)
(* ------------- *)

(* unsafe *)
let array_zip n op a1 a2 =
  if n=0 then [||] else
    let res=Array.create n (op a1.(0) a2.(0)) in
    for i = 1 to pred n do
      Array.unsafe_set res i (op (Array.unsafe_get a1 i) (Array.unsafe_get a2 i))
    done;
    res
      
let (zip_treeflow : (('a -> 'b -> 'c) -> 'a treeflow -> 'b treeflow -> 'c treeflow)) f (t1,n1) (t2,n2) =
  let multi tree zip () =
    where node =
      let rc = << zip () >> in
      let res = array_zip n1 f (at_b t1) (at_b t2) in
      finally rc res
    where leaf =
      array_zip n1 f (at_s t1) (at_s t2)
  in
  if n1<>n2 then
    invalid_arg "zip"
  else
    (zip (),n1)

let (zip_leafflow : (('a -> 'b -> 'c) -> 'a leafflow -> 'b leafflow -> 'c leafflow)) f (t1,n1) (t2,n2) =
  let multi tree zip () =
    where node =
      let rc = << zip () >> in
      finally rc None
    where leaf =
      Some (array_zip n1 f (at_s t1) (at_s t2))
  in
  if n1<>n2 then
    invalid_arg "zip"
  else
    (zip (),n1)


               
(* ------------- *)
(* --- REDUCE -- *)
(* ------------- *)

let flatten v = let p = proj v in Array.concat (List.map (fun i -> p i) (from_to 0 (nb_children()-1)));;
      
(*(('a -> 'a -> 'a) -> 'a -> 'a treeflow -> 'a)*)
let reduce_treeflow op e (t,n) =     
  let multi reduce t =
    where node =
      let rc = << reduce t >> in
      let here = Array.fold_left op e (at t) in
      let res = Array.fold_left op here (flatten rc) in
      res
    where leaf =
        let res = Array.fold_left op e (at t) in
        res
 in reduce t;;

(* (('a -> 'a -> 'a) -> 'a -> 'a leafflow -> 'a) *)      
let reduce_leafflow op e (t,n) =     
  let multi reduce t =
    where node =
      let rc = << reduce t >> in
      let res = Array.fold_left op e (flatten rc) in
      res
    where leaf =
        let res = Array.fold_left op e (at t) in
        res
 in reduce t;;



  
(* ------------- *)
(* ---- SCAN --- *)
(* ------------- *)

let _ = todo ();;


(* ------------- *)
(* ----- DH ---- *)
(* ------------- *)

  
