type 'a flow

val length : 'a flow -> int

val of_list : 'a list -> 'a flow

val to_list : 'a flow -> 'a list

val map : ('a -> 'b) -> 'a flow -> 'b flow

val mapidx : (int -> 'a -> 'b) -> 'a flow -> 'b flow

val zip : ('a -> 'b -> 'c) -> 'a flow -> 'b flow -> 'c flow

val reduce : ('a -> 'a -> 'a) -> 'a -> 'a flow -> 'a

val scan : ('a -> 'a -> 'a) -> 'a -> 'a flow -> 'a flow

val dh : ('a -> 'a -> 'a) -> ('a -> 'a -> 'a) -> 'a flow -> 'a flow

val repl : 'a -> int -> 'a flow
