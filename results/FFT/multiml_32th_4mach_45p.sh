BENCH=~/COMPIL_DIR/Multi-ML-Bench
SRC=$BENCH/Skeletons/benchs/MultiML/fft
HST=$BENCH/hostfiles/mirev2/mirev2_32th_4mach_45p_MultiML
SCRIPT=$BENCH/scripts/generic.sh

$SCRIPT fft_multiml_10.bench 10 $HST $SRC'/fft 10'
$SCRIPT fft_multiml_15.bench 10 $HST $SRC'/fft 15'
$SCRIPT fft_multiml_18.bench 10 $HST $SRC'/fft 18'
$SCRIPT fft_multiml_20.bench 10 $HST $SRC'/fft 20'
$SCRIPT fft_multiml_21.bench 5 $HST $SRC'/fft 21'
$SCRIPT fft_multiml_22.bench 5 $HST $SRC'/fft 22'
