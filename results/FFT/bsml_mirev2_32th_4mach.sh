BENCH=~/COMPIL_DIR/Multi-ML-Bench
SRC=$BENCH/Skeletons/benchs/BSML/fft
HST=$BENCH/hostfiles/mirev2/mirev2_32th_4mach_BSML
SCRIPT=$BENCH/scripts/generic.sh

$SCRIPT fft_bsml_mirev2_10.bench 10 $HST $SRC'/fft 10'
$SCRIPT fft_bsml_mirev2_15.bench 10 $HST $SRC'/fft 15'
$SCRIPT fft_bsml_mirev2_18.bench 10 $HST $SRC'/fft 18'
$SCRIPT fft_bsml_mirev2_20.bench 10 $HST $SRC'/fft 20'
$SCRIPT fft_bsml_mirev2_21.bench 5 $HST $SRC'/fft 21'
$SCRIPT fft_bsml_mirev2_22.bench 5 $HST $SRC'/fft 22'
$SCRIPT fft_bsml_mirev2_23.bench 5 $HST $SRC'/fft 23'
