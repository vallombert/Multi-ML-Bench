module type MATRIX =
  sig
    
    type 'a matrix

    val init : int -> int -> (int -> 'a) -> 'a matrix
                                               
    val at : 'a matrix -> int -> int -> 'a

    val set : 'a matrix -> int -> int -> 'a -> unit
                                                 
    val display : int matrix -> unit

    val sub_matrix : 'a matrix -> int -> int -> int -> int -> 'a matrix
                                                                 
    val add_int : int matrix -> int matrix -> int matrix -> int -> int -> int -> unit

    val mult_int : int matrix -> int matrix -> int matrix -> int -> int -> int -> unit

    val mult : int matrix -> int -> int ->
               int matrix -> int -> int ->
               int matrix -> int -> int ->
               int -> unit
                        
    val mult_block : int matrix -> int -> int ->
                     int matrix -> int -> int ->
                     int matrix -> int -> int ->
                     int -> int -> unit

    val block_iter : int matrix -> int matrix -> int matrix -> int -> unit

    val write_block : 'a matrix -> int -> int ->
                      'a matrix -> int -> int ->
                      int -> unit
                                                                        
    (**Optimal blok size for Matrix Multiplication*)
    val __blk_size : int
  end
