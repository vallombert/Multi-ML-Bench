include Matrix_1d.Matrix;;

let _ =
  let n = 1600 in
  let a = init n n (fun i -> i / n) in
  let b = init n n (fun i -> i mod n) in
  let c = init n n (fun _ -> 0) in
  (* mult_int a b c 0 0 n; *)
  let start = Unix.gettimeofday () in
  mult_block a 0 0 b 0 0 c 0 0 __blk_size n;
  let stop1 = Unix.gettimeofday () in
  Printf.printf "Total Execution time: %fs\n" (stop1 -. start)
