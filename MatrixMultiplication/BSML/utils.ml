let from_to n1 n2 = 
  let rec from_to' n1 accu =  
    if n1>n2 then accu else (from_to' (n1+1) (n1::accu)) in
  List.rev (from_to' n1 [])
           
let nprocs = Bsml.bsp_p
let procs = from_to 0 (nprocs-1)
                    
let id_to_topo pid nprocs =
  let root = (int_of_float (sqrt (float_of_int nprocs))) in
  (pid / root,pid mod root)

let topo_to_id topo nprocs =
  let root = (int_of_float (sqrt (float_of_int nprocs))) in
  (fst topo)*root + snd topo
