include Matrix_1d.Matrix
open Bsml

                    
(**)
                          
let max_of exectime nprocs =
  let pr = proj exectime in
  let restime = Array.init (nprocs) (fun i -> pr i) in
  Array.fold_left (max) (Array.get restime 0) restime
                  
let _ =
  let n = 1600 in
  let blk_size = int_of_float ((float_of_int n) /. sqrt (float_of_int Utils.nprocs)) in
  (* Printf.printf "blk_size %i\n%!" (blk_size); *)
  let a = init n n (fun i -> i / n) in
  let b = init n n (fun i -> i mod n) in
  let c = init n n (fun _ -> 0) in

  let topo = << Utils.id_to_topo $this$ Utils.nprocs >> in
  (* let _ = << Printf.printf "Domain of %i is : (%i,%i)\n%!" $this$ $sub_rid$ $sub_cid$ >> in *)
  let sub_rows = << sub_matrix a ((fst $topo$)*blk_size) 0 blk_size n >> in
  (* let _ = << Printf.printf "A of pid %i:\n" $this$ ; display $sub_rows$ >> in *)
  let sub_cols = << sub_matrix b 0 ((snd $topo$)*blk_size) n blk_size >> in
  (* let _ = << Printf.printf "B of pid %i:\n" $this$ ; display $sub_cols$ >> in *)
  (*The resulting matrix*)
  let res = << init blk_size blk_size (fun _ -> 0) >> in
  
  let start = Unix.gettimeofday () in
  let _ = << block_iter $sub_rows$ $sub_cols$ $res$ blk_size >> in
  let _ = proj << () >> in
  let stop1 = Unix.gettimeofday () in
  let prj = proj res in
  let stop2 = Unix.gettimeofday () in
  let _ = matrix_fuse prj c in
  let _ = proj << () >> in
  let stop3 = Unix.gettimeofday () in
  
  (*TODO write_block: to build final matrix*)
  (* let _ =  << Printf.printf "Pid %i\n" $this$; display $res$; print_string "\n">> in *)
  
  let exectime = << stop1 -. start >> in
  let commtime = << stop2 -. stop1 >> in
  let bldmattime = << stop3 -. stop2 >> in
  let maxcomp = max_of exectime Utils.nprocs in
  let maxcomm = max_of commtime Utils.nprocs in
  let maxbldmat = max_of commtime Utils.nprocs in
  
  <<if $this$ = (Utils.nprocs-1) then Printf.printf "Max computation time: %fs\n" maxcomp >>;
  <<if $this$ = (Utils.nprocs-1) then Printf.printf "Max communication time: %fs\n" maxcomm >>;
  <<if $this$ = (Utils.nprocs-1) then Printf.printf "Fuse results time: %fs\n" maxbldmat >>;
  <<if $this$ = (Utils.nprocs-1) then Printf.printf "Total Execution time: %fs\n%!" (maxcomp+.maxcomm+.maxbldmat) >>

  ;proj << () >>
    (*Check result*)
    (* ; *)
    (*   << if $this$ = 0 then  *)
    (*    let check = init n n (fun _ -> 0) in *)
    (*   let _ = mult_int a b check 0 0 n in *)
    (*   for i = 0 to n-1 do *)
    (*     for j = 0 to n-1 do *)
    (*       if at c i j <> at check i j then *)
    (*         failwith "Resulting matrix is false" *)
    (*     done *)
    (*   done  *)
    (*    else () *)
    (*    >> *)
    
    (* let _ = *)
    (*   let f n = function *)
    (*     | i -> init n n  (fun _ -> i+1) *)
    (*   in  *)
    (*   let c = init 4 4 (fun _ -> 42) in *)
    (*   matrix_fuse (f 2) c; *)
    (*   display c *)
