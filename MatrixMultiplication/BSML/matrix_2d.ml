module Matrix : Matrixsig.MATRIX =
  struct

    type 'a matrix = { data : 'a array array; rows : int; cols : int}

    let __blk_size = 2
                       
    let init h w f =
      {data=Array.make_matrix h w (f 0);rows=h;cols=w}
        
    let at m i j =
      m.data.(i).(j)

    let set m i j v =
      m.data.(i).(j)<-v

    let display m =
      let r = m.rows in
      let c = m.cols in
      for i = 0 to r-1 do
        for j = 0 to c-1 do
          Printf.printf "%i  " (at m i j);
        done;
        Printf.printf "\n";
      done

    let sub_matrix m row col srow scol =
      let res = init srow scol (fun _ -> at m 0 0) in
      for i = 0 to srow-1 do
        for j = 0 to scol-1 do
          set res i j (at m (row+i) (col+j))
        done
      done;
      res

        
    let add_int a b c row col stride =
      assert (a.rows = c.rows);
      assert (b.rows = c.rows);
      for i = row to row+stride-1 do
        for j = col to col+stride -1 do
          set c i j (at a i j + at b i j)
        done
      done

    (*Todo: strassen*)
    let mult_int a b c row col stride =
      assert (a.rows = c.rows);
      assert (b.rows = c.rows);
      for i = row to row+stride-1 do
        for j = col to col+stride-1 do
          for k = 0 to stride-1 do
            set c i j ((at c i j) + (at a i (col+k)) * (at b (row+k) j));
          done
        done
      done

    let mult_block a ar ac b br bc c cr cc blk dim =
      assert (dim mod blk = 0);
      let i = ref 0 and j = ref 0 and k = ref 0 in
      while !i < dim-1 do
        j:=0;
        while !j < dim-1 do
          k:=0;
          while !k < dim-1 do
            (*Do MM*)
            for x = !i to !i+blk-1 do
              for y = !j to !j+blk-1 do
                for z = !k to !k+blk-1 do
                  set c (x+cr) (y+cc) ((at c (x+cr) (y+cr)) + (at a (x+ar) (z+ac)) * (at b (z+br) (y+bc)));
                done
              done
            done;
            k:=!k+blk
          done;
          j:=!j+blk
        done;
        i:=!i+blk
      done


    let mult a ar ac b br bc c cr cc dim =
      for x = 0 to dim-1 do
        for y = 0 to dim-1 do
          for z = 0 to dim-1 do
            set c (x+cr) (y+cc) ((at c (x+cr) (y+cc)) + (at a (x+ar) (z+ac)) * (at b (z+br) (y+bc)));
          done
        done
      done


    let block_iter a b c blk =
      assert(a.rows = b.cols);
      assert(a.cols = b.rows);
      assert(a.rows = c.rows);
      assert(b.cols = c.cols);
      let i = ref 0 and j = ref 0 in
      while !i < a.rows-1 do
        j:=0;
        while !j < a.cols-1 do
          mult_block a !i !j b !j !i c (!i mod a.rows) (!j mod a.rows) __blk_size blk;
          j:=!j+blk;
        done;
        i:=!i+blk;
      done



    let write_block src rs cs dst rd cd dim =
      assert (src.rows >= dst.rows);
      assert (src.cols >= dst.cols);
      for i = 0 to dim -1 do
        for j = 0 to dim -1 do
          set dst (i+rd) (j+cd) (at src (i+rs) (j+cs))
        done
      done

        
    let matrix_fuse f out =
      let fuse pid =
        let topo = Utils.id_to_topo pid Utils.nprocs in
        let data = f pid in
        let dim = data.rows in
        write_block data 0 0 out ((fst topo)*dim) ((snd topo)*dim) dim
      in ignore (List.map fuse Utils.procs)
                
  end
