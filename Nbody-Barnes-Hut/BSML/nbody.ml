open Graphics;;
open Body;;
open Utils;;
open Bsml;;

(*Window parameters*)
let w_size = 800.0
let half_w_size = w_size /. 2.
                              
(*Input file*)
let infile = 
  let file = Sys.argv.(1) in
  open_in file
          
(*Number of praticles*)
let n = int_of_string (input_line infile)

(*Radius of univers*)
let radius_universe = float_of_string (input_line infile)

(*Compute max coord*)
let get_max_coord bodies =
  let max = ref 0.0 in
  let _ =
    Array.iter
      (fun b ->
        if abs_float !(!b.rx) > !max then max := abs_float !(!b.rx)
        else
          if abs_float !(!b.ry) > !max then max := abs_float !(!b.ry))
      bodies;
  in  
  Printf.printf "borne supp %f\n" !max;
  !max

(*Simulation steps*)
let steps = 100.
(*Current time*)
let t = ref 0.0
(*Time delta*)
let dt = 0.1
           
let _ =
  (*Open display window*)
  open_graph (" "^(string_of_int (int_of_float w_size))^"x"^(string_of_int (int_of_float w_size)));

  (*This version is working with 4 th only ! *)
  let _ = if bsp_p != 4 then failwith "NP = 4 only" in


(* /!\ The distribution is not optimised yet /!\*)
(*The bodies of the whole system*)
let bodies = init_bodies infile n in
    let universe = (Quadtree.mk_quad 0.0 0.0 (radius_universe *. 2.0)) in
    let my_quad = << (Quadtree.select_quad $this$) universe >> in
    (*The whole quadtree*)
    let qd3s = << Quadtree.build_quadtree bodies (ref (Quadtree.Node (Quadtree.mk_empty_quadtree universe))) >> in
    (*My own bodies*)
    let qd3s = << Quadtree.select_bodies $qd3s$ $this$ >> in
    let my_bodies = << Array.of_list (Quadtree.extract_bodies $qd3s$) >> in
    let _ = << Quadtree.print_quad $my_quad$ >> in

    let max_coord = get_max_coord bodies in

    (*Graphical display*)
    clear_graph();
    set_color red;
    fill_rect (round half_w_size) (round half_w_size) 5 5;
    << Visu.print_quadtree !($qd3s$) 3 half_w_size (max_coord) >>;
    
    while !t < steps do
      
      Printf.printf "--------------------------------------------\n--------------------------------------------\n";
      let tmp_pos = << Array.make (Array.length $my_bodies$) (0.0,0.0) >> in
      
      let _ =
        <<
         clear_graph();
         set_color red;
         fill_rect (round half_w_size) (round half_w_size) 5 5;
         Visu.print_quadtree !($qd3s$) 3 half_w_size (max_coord) 
         >>
      in 

      
      let _ =
        <<
         Array.iteri
         (fun i b ->

         Body.reset_bodies_f $my_bodies$;
         Quadtree.update_force b $qd3s$;
         (*Compute new pos and velocity*)
         Array.set $tmp_pos$ i (!((!b).fx),!((!b).fy));
         (* Quadtree.update b dt *)

         )
         $my_bodies$
         >>
      in
      
      (* read_int(); *)
      (*Testing*)

      << Array.iter (fun x -> Printf.printf "%i -> " $this$; print_body !x) $my_bodies$>>;
      
      << List.iter (fun x -> Printf.printf "Pid %i -> send %i bodies to %i\n" $this$ (List.length (Hashtbl.find_all Quadtree.bucket x)) x) nprocs >>;
      (*Send and receive missing data*)
      let to_send = << fun x -> Hashtbl.find_all Quadtree.bucket x >> in
      let distant_data = put to_send in
      << List.iter (fun x -> Printf.printf "Pid %i -> need %i bodies from %i\n" $this$ (List.length ($distant_data$ x) ) x) nprocs >>;

      
      (*Compute needed data*)
      let needed_data = << List.map (fun x -> 
                         let bdy = ($distant_data$ x) in 
                         List.map (fun b -> Printf.printf "compute %i:" $this$; print_body b ;(b,Quadtree.distant_force b $qd3s$)) bdy )
                         nprocs >> in

      (* let _ = << if $this$ = 1 then  *)
      (*          begin *)
      (*          let dbys = $distant_data$ 3 in *)
      (*          let b = List.nth dbys 0 in  *)
      (*          let infl = Quadtree.distant_force b $qd3s$ in *)
      (*          Printf.printf " ********************************Comupting …\n"; *)
      (*          print_body b; *)
      (*          Printf.printf "(%10.3E,%10.3E)\n" (fst infl) (snd infl); *)
      (*          Printf.printf "-----------\n" *)
      (*          (\* let b = Quadtree.distant_force *\) *)
      (*          end *)
      (*          >> in *)

      let _= <<
              Printf.printf "I am %i\n" $this$;
              List.map ( fun y ->
              (fun x -> Printf.printf "%i values sent to %i\n" (List.length x) y)
              (List.nth $needed_data$ y)) nprocs
              >> in

      let _= <<
              Printf.printf "I am %i\n" $this$;
              List.map ( fun y ->
              Printf.printf " ***** Computed data (%i)*****\n" y;
              List.map
              (fun x ->              
               print_body (fst x);
               Printf.printf "(%10.3E,%10.3E)\n" (fst (snd x)) (snd (snd x));)
              (List.nth $needed_data$ y)) nprocs
              >> in
      
      
      (*Send and receive needed data*)
      let to_send = << fun x -> List.nth $needed_data$ x >> in
      let new_data = put to_send in

      let _= <<
              Printf.printf "I am %i\n" $this$;
              List.map
              (fun x ->
              Printf.printf "%i values received from %i\n" (List.length ( $new_data$ x)) x)
              nprocs
              >> in
      

      
          let _ =
            <<
             (* Array.iteri *)
             (* (fun i b -> *)
             (*  Body.reset_bodies_f $my_bodies$; *)
             (* Printf.printf "Before:\n"; *)
             (* print_body !($my_bodies$.(i)); *)

             List.map 
             ( fun id -> 
               List.iter (
                 fun x -> 
                   let force = (snd x) in let bdy = fst x in
                   (Array.iteri (fun i b -> 
                       if equals b (ref bdy) then 
                           begin
                           let tmp = Array.get $tmp_pos$ i in
                           Array.set $tmp_pos$ i (fst tmp +. fst force,snd tmp +. snd force)
                           end
                       else ()) 
                     $my_bodies$);
               )
               ($new_data$ id)
             )
             nprocs;

             Array.iteri
             (fun i b -> 
             let tmp = Array.get $tmp_pos$ i in
             (!b).fx := fst tmp;
             (!b).fy := snd tmp;
             Quadtree.update b dt ;
             Body.reset_bodies_f $my_bodies$
             )
             $my_bodies$


             (* let vx,vy,rx,ry = Quadtree.compute_update b dt in *)
             (* let vx',vy',rx',ry' = Array.get $tmp_pos$ i in *)
             (* !b.vx := vx +. vx'; *)
             (* !b.vy := vy +. vy'; *)
             (* !b.rx := rx +. rx'; *)
             (* !b.ry := ry +. ry'; *)
             (* Printf.printf "After:\n"; *)
             (* print_body !($my_bodies$.(i)); *)
             (* Body.reset_bodies_f $my_bodies$ *)
             (* ) *)
             (* $my_bodies$ *)
             >>
          in

          (*Testing*)
          (* read_int();  *)
          (*Testing*)
          
      t := !t +. dt;
      Utils.wait_ms 1000.
    done;
    
