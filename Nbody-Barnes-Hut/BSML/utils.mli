val gravity_cst : float
val theta : float
val eps : float
val eps2 : float
val round : float -> int
val is_none : 'a option -> bool
val is_some : 'a option -> bool
exception None_of_option
val noneSome : 'a option -> 'a
val wait_ms : float -> unit
val pid : int Bsml.par
val nprocs : int list
