val draw_circle_float : float -> float -> int -> float -> float -> unit
val draw_bodies : Body.body list -> int -> float -> float -> unit
val print_quadtree : Quadtree.quadtree -> int -> float -> float -> unit
