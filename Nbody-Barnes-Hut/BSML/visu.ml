open Graphics;;
open Utils;;
open Body;;
open Quadtree;;

let draw_circle_float (x:float) (y:float) (r:int) (stride:float) (max_coord:float)=
  fill_circle (round (x *. stride /. max_coord +. stride)) (round (y *. stride /. max_coord +. stride)) r
              
let draw_bodies bodies radius stride max_coord =
  List.iter (fun b -> draw_circle_float !(b.rx) !(b.ry) radius stride max_coord) bodies

(*Print quadtree dimensions*)
let rec print_quadtree oc3 radius stride max_coord =
  begin
    match oc3 with
      Empty -> ()
    | Node n when (is_external n) ->
       begin
         match !(n.body) with
           Some b ->
           set_color black;
           draw_circle_float !(!b.rx) !(!b.ry) radius stride max_coord
         | None -> ()
       end 
    | Node n ->
       (*Display gravity centers*)
       (* begin *)
       (*   match !(n.body) with *)
       (*     Some b -> *)
       (*     print_body b; *)
       (*     set_color green; *)
       (*     draw_circle_float !(b.rx) !(b.ry) radius stride max_coord *)
       (*   | None -> () *)
       (* end; *)
       set_color blue;
       moveto (round ((n.quad.xmid -. n.quad.length/.2.0) *. stride /. max_coord +. stride)) (round (n.quad.ymid *. stride /. max_coord +. stride));
       lineto (round ((n.quad.xmid +. n.quad.length/.2.0) *. stride /. max_coord +. stride)) (round (n.quad.ymid *. stride /. max_coord +. stride)) ;
       moveto (round (n.quad.xmid *. stride /. max_coord +. stride)) (round ((n.quad.ymid -. n.quad.length/.2.0) *. stride /. max_coord +. stride));
       lineto (round (n.quad.xmid *. stride /. max_coord +. stride)) (round ((n.quad.ymid +. n.quad.length/.2.0) *. stride /. max_coord +. stride));
       print_quadtree !(n.nw) radius stride max_coord;
       print_quadtree !(n.ne) radius stride max_coord;
       print_quadtree !(n.sw) radius stride max_coord;
       print_quadtree !(n.se) radius stride max_coord;
    | Distantnode n ->
       set_color red;
       moveto (round ((n.quad.xmid -. n.quad.length/.2.0) *. stride /. max_coord +. stride)) (round (n.quad.ymid *. stride /. max_coord +. stride +.stride/.2.0));
       lineto (round ((n.quad.xmid +. n.quad.length/.2.0) *. stride /. max_coord +. stride)) (round (n.quad.ymid *. stride /. max_coord +. stride -.stride/.2.0)) ;
       moveto (round (n.quad.xmid *. stride /. max_coord +. stride -.stride/.2.0)) (round ((n.quad.ymid -. n.quad.length/.2.0) *. stride /. max_coord +. stride));
       lineto (round (n.quad.xmid *. stride /. max_coord +. stride +.stride/.2.0)) (round ((n.quad.ymid +. n.quad.length/.2.0) *. stride /. max_coord +. stride));
  end
