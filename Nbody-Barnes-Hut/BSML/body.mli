type body = {
  rx : float ref;
  ry : float ref;
  vx : float ref;
  vy : float ref;
  fx : float ref;
  fy : float ref;
  mass : float;
}
val mk_body :
  float ref ->
  float ref ->
  float ref -> float ref -> float ref -> float ref -> float -> body
val up_body_r : body -> float -> float -> unit
val up_body_v : body -> float -> float -> unit
val up_body_f : body -> float -> float -> unit
val reset_body_f : body -> unit
val reset_bodies_f : body ref array -> unit
val mk_body_from_lst : string list -> body
val init_bodies : in_channel -> int -> body ref array
val print_body : body -> unit
val equals : body ref -> body ref -> bool
