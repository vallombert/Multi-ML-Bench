type quad = { xmid : float; ymid : float; length : float; }
val mk_quad : float -> float -> float -> quad
val print_quad : quad -> unit
type quadtree = Empty | Node of quadtreenode | Distantnode of distoctreenode
and quadtreenode = {
  body : Body.body ref option ref;
  mass : float;
  quad : quad;
  nw : quadtree ref;
  ne : quadtree ref;
  sw : quadtree ref;
  se : quadtree ref;
}
and distoctreenode = {
  pid : int;
  body : Body.body ref option;
  mass : float;
  quad : quad;
}
val mk_full_quadtreenode :
  Body.body ref option ref ->
  float ->
  quad ->
  quadtree ref ->
  quadtree ref -> quadtree ref -> quadtree ref -> quadtreenode
val mk_quadtreenode :
  float ->
  quad ->
  quadtree ref ->
  quadtree ref -> quadtree ref -> quadtree ref -> quadtreenode
val mk_empty_quadtree : quad -> quadtreenode
val mk_distantnode : int -> Body.body ref option -> float -> quad -> quadtree
val is_external : quadtreenode -> bool
val print_quadtree_txt : quadtree -> string -> unit
val within : Body.body -> quad -> bool
val add_to_body : Body.body -> Body.body -> Body.body
val mk_quad_nw : quad -> quad
val mk_quad_ne : quad -> quad
val mk_quad_sw : quad -> quad
val mk_quad_se : quad -> quad
val put_body : Body.body ref option ref -> quadtree ref -> unit
val insert_quadtree : Body.body ref -> quadtree ref -> unit
val compute_force : Body.body ref -> Body.body ref -> float * float
val add_force : Body.body ref -> Body.body ref -> unit
val distance : Body.body -> Body.body -> float
val bucket : (int, Body.body) Hashtbl.t
val update_force : Body.body ref -> quadtree ref -> unit
val compute_updated_v : Body.body ref -> float -> float * float
val compute_updated_r :
  Body.body ref -> float -> float -> float -> float * float
val compute_update : Body.body ref -> float -> float * float * float * float
val update : Body.body ref -> float -> unit
val select_quad : int -> quad -> quad
val build_quadtree : Body.body ref array -> quadtree ref -> quadtree ref
val get_body : quadtree -> Body.body ref option * quad * float
val dn_from_node : int -> quadtree ref -> quadtree ref
val select_bodies : quadtree ref -> int -> quadtree ref
val extract_bodies : quadtree ref -> Body.body ref list
val distant_force : Body.body -> quadtree ref -> float * float
val add_force_to_body : Body.body -> quadtree ref -> float * float -> unit
