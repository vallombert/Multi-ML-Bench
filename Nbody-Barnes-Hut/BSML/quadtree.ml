open Utils;;
open Body;;
  
(*Qadrant*)
type quad = {
    xmid:float; (*center of the quandrant*)
    ymid:float;
    length:float (*quandrant length*)
  }

let mk_quad xmid ymid length =
  {xmid;ymid;length}

let print_quad q =
  Printf.printf "xmid=%f,ymid=%f,len=%f\n" q.xmid q.ymid q.length
                
(*Quadtree*)
type quadtree =
  | Empty
  | Node of quadtreenode
  | Distantnode of distoctreenode 
   and
     quadtreenode = {
       body:Body.body ref option ref;
       mass:float; (*Mass of quadtree node*)
       quad:quad;
       nw:quadtree ref;   (*Leaves*)
       ne:quadtree ref;
       sw:quadtree ref;
       se:quadtree ref;
     }
   and
     distoctreenode = {
       pid:int;
       body:Body.body ref option;
       mass:float;
       quad:quad;
     }                


(*Built quadtreenode*)
let mk_full_quadtreenode body mass quad nw ne sw se =
  {body;mass;quad;nw;ne;sw;se}    
    
(*Built quadtreenode*)
let mk_quadtreenode mass quad nw ne sw se =
  {body=ref None;mass;quad;nw;ne;sw;se}
    
let mk_empty_quadtree quad =
  {body=ref None;mass=0.0;quad;nw=ref Empty;ne=ref Empty;sw=ref Empty;se=ref Empty}

(*Make a distant node*)
let mk_distantnode pid body mass quad=
  Distantnode {pid;body;mass;quad}
              
(*Test if the node is, basically, a leaf*)
let is_external n =
  !(n.nw) = Empty && !(n.ne) = Empty && !(n.sw) = Empty && !(n.se) = Empty

(*Print tree as a leaf*)
let rec print_quadtree_txt oc3 str =
  begin
    match oc3 with
    | Empty -> Printf.printf "%sEmpty\n" str
    | Node n when (is_external n) ->
       Printf.printf "%sLeaf\n" str
    | Node n ->
       Printf.printf "%sNode\n" str;
       print_quadtree_txt !(n.nw) (str^" ");
       print_quadtree_txt !(n.ne) (str^" ");
       print_quadtree_txt !(n.sw) (str^" ");
       print_quadtree_txt !(n.se) (str^" ");
    | Distantnode _ -> ()
  end
    
(*Test if a body is inside a quad*)
let within body quad =
  let hlen = quad.length /. 2.0 in
  (!(body.rx) >= quad.xmid -. hlen ) && (!(body.rx) <= quad.xmid +. hlen)
  && (!(body.ry) >= quad.ymid -. hlen) && (!(body.ry) <= quad.ymid +. hlen) 


(*Bulild a new body from two bodies*)
let add_to_body (new_body:Body.body) (current_body:Body.body) =
  let m = current_body.Body.mass +. new_body.Body.mass in
  let x = (!(current_body.rx) *. current_body.Body.mass +. !(new_body.rx) *. new_body.Body.mass) /. m in
  let y = (!(current_body.ry) *. current_body.Body.mass +. !(new_body.ry) *. new_body.Body.mass) /. m in
  let vx = !(current_body.vx) in
  let vy = !(current_body.vy) in
  let out = mk_body (ref x) (ref y) (ref vx) (ref vy) (ref 0.0) (ref 0.0) m in out


(*Build quadrants*)
let  mk_quad_nw q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid -. q.length /. 4.0) (q.ymid +. q.length /. 4.0) (hlen)

let  mk_quad_ne q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid +. q.length /. 4.0) (q.ymid +. q.length /. 4.0) (hlen)

let  mk_quad_sw q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid -. q.length /. 4.0) (q.ymid -. q.length /. 4.0) (hlen)

let  mk_quad_se q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid +. q.length /. 4.0) (q.ymid -. q.length /. 4.0) (hlen)

(*Puts a body within the right quadrant*)
let rec put_body (b:Body.body ref option ref) (tree:quadtree ref) =
  let node = (function | Node n -> n | Empty | Distantnode _ -> failwith "put_body quadtree fail") (!tree) in
  let b = Utils.noneSome !b in
  let nw = mk_quad_nw node.quad in
  let ne = mk_quad_ne node.quad in
  let sw = mk_quad_sw node.quad in
  let se = mk_quad_se node.quad in
  (*Select the quadrant*)
  if within !b nw then
    insert_quadtree (b) node.nw
  else
    if within !b ne then
      insert_quadtree (b) node.ne
    else
      if within !b se then
        insert_quadtree (b) node.se
      else
        if within !b sw then
          insert_quadtree (b) node.sw

(*Insert a body inside a quadtree*)
and insert_quadtree (body:Body.body ref) (tree:quadtree ref) =
  begin
    match !tree with
      Node n ->
      (*body of node is empty*)
      if is_none !(n.body) then
        begin
          n.body := Some (ref !body)
        end
      else
        (*Internal node*)
        if not (is_external n) then
          begin
            n.body := Some (ref (add_to_body !(noneSome (!(n.body))) !body));
            put_body (ref (Some body)) tree
          end
        else
          begin
            let nw = mk_quad_nw n.quad in
            let ne = mk_quad_ne n.quad in
            let sw = mk_quad_sw n.quad in
            let se = mk_quad_se n.quad in
            n.nw := Node (mk_empty_quadtree nw);
            n.ne := Node (mk_empty_quadtree ne);
            n.se := Node (mk_empty_quadtree se);
            n.sw := Node (mk_empty_quadtree sw);
            put_body n.body tree;
            put_body (ref (Some body)) tree;
            n.body := Some (ref (add_to_body !(noneSome (!(n.body))) !body));
          end
    | Distantnode _ -> ()
    | Empty -> failwith "Empty is impossible !! Oh wait …"
  end
    
(*compute force of body B to body A*)          
let compute_force (a:Body.body ref) (b:Body.body ref) =
  print_body !a;
  print_body !b;
  let dx = !(!b.rx) -. !(!a.rx) in
  let dy = !(!b.ry) -. !(!a.ry) in
  let dist = sqrt (dx *. dx +. dy *. dy) in
  let force = (Utils.gravity_cst *. !a.Body.mass *. !b.Body.mass) /. (dist *. dist +. eps2) in
  let fx = force *. dx /. dist in
  let fy = force *. dy /. dist in
  (fx,fy)
    
(*add force of body B to body A*)
let add_force (a:Body.body ref) (b:Body.body ref) =
  let fx,fy = compute_force a b in
  !a.fx := !(!a.fx) +. fx;
  !a.fy := !(!a.fy) +. fy;
  ()
    
(*Distance from a to b*)
let distance (a:Body.body) (b:Body.body) =
  let dx = !(a.rx) -. !(b.rx) in
  let dy = !(a.ry) -. !(b.ry) in
  sqrt (dx*.dx +. dy*.dy)

(*The bucket is used to store the bodies wich need distant data*)
let (bucket: (int,Body.body) Hashtbl.t) = Hashtbl.create 1000
                                                         
(*Compute forces*)
let rec update_force (body:Body.body ref) (tree:quadtree ref) =
  begin
    match !tree with
      Empty -> failwith "Empty is not possible ! Oh wait …" 
     |Node n ->
       (*Empty node or same node*)
       if (is_none (!(n.body))) || Body.equals (noneSome (!(n.body))) body then (
         ()
       )
       else
         (*Node is external*)
         if is_external n then
           begin
             (*adds the force of n.body to body*)
             add_force body (noneSome !(n.body))
           end
         else
           begin
             (*Check if the quadtree node is not too far*)
             let s = n.quad.length in
             let d = distance !(noneSome !(n.body)) !body in
             if (s/.d) < Utils.theta then (
               add_force body (noneSome !(n.body))
             )
             else
               begin
                 update_force body n.nw;
                 update_force body n.ne;
                 update_force body n.sw;
                 update_force body n.se;
               end
           end
     | Distantnode n ->
        Printf.printf "-----> Distant node\n" ;
        (* failwith "yolo"; *)
        let s = n.quad.length in
        let dn = noneSome (n.body) in
        let d = distance !dn !body in
        if (s/.d) < Utils.theta then (
          Printf.printf "is far away\n";
          add_force body dn
        )
        else
          begin
            Hashtbl.add bucket n.pid !body;
          end
  end

let compute_updated_v (body:Body.body ref) (dt:float) =
  let vx = !(!body.vx) +. ( dt *. !(!body.fx) /. !body.mass) in
  let vy = !(!body.vy) +. ( dt *. !(!body.fy) /. !body.mass) in
  (vx,vy)

let compute_updated_r (body:Body.body ref) (dt:float) vx vy=
  let rx = !(!body.rx) +. (vx *. dt) in
  let ry = !(!body.ry) +. (vy *. dt) in
  (rx,ry)

(*Compute bodies update after force calculation*)
let compute_update (body:Body.body ref) (dt:float) =
  let vx,vy = compute_updated_v body dt in
  let rx,ry = compute_updated_r body dt vx vy in
  (vx,vy,rx,ry)
    
(*Update bodies after force calculation*)
let update (body:Body.body ref) (dt:float) =
  !body.vx := !(!body.vx) +. ( dt *. !(!body.fx) /. !body.mass);
  !body.vy := !(!body.vy) +. ( dt *. !(!body.fy) /. !body.mass);
  !body.rx := !(!body.rx) +. (!(!body.vx) *. dt);
  !body.ry := !(!body.ry) +. (!(!body.vy) *. dt)

(* /!\ Unoptimised/4thread version /!\*)
(*Select a quand from a pid*)
let select_quad = function
  | 0 -> mk_quad_nw
  | 1 -> mk_quad_ne
  | 2 -> mk_quad_se
  | 3 -> mk_quad_sw
  | _ -> failwith "Version limited to 4 Threads only"

(* /!\ Unoptimised/4thread version /!\*)
(*Build quadtree from bodies*)
let build_quadtree (bodies:Body.body ref array) (tree:quadtree ref) =
  Array.iter (fun b ->
      insert_quadtree b tree
    ) bodies;
  tree

(* /!\ Unoptimised/4thread version /!\*)
(*Returns a body. Fails otherwise*)
let get_body n =
  begin
    match n with
      Node n -> (!(n.body),n.quad,n.mass)
     |_ -> failwith "yolooooo"
  end

    
(*Make distant node from node*)
let dn_from_node pid n =
  let b,q,m = get_body !n in
  let b = Some (ref (mk_body (ref q.xmid) (ref q.ymid) (ref 0.0) (ref 0.0) (ref 0.0) (ref 0.0) 0.0)) in
  ref (mk_distantnode pid b m q)
      
(* /!\ Unoptimised/4thread version /!\*)
(*Extract sub-tree depending on a pid*)
let select_bodies qd3 id = 
  let n = (function | Node n -> n | Empty | Distantnode _ -> failwith "Nope") !qd3 in
    begin
      match id with
        0 -> ref (Node (mk_full_quadtreenode n.body n.mass n.quad
                                             n.nw
                                             (dn_from_node 1 n.ne)
                                             (dn_from_node 2 n.se)
                                             (dn_from_node 3 n.sw)))
      | 1 -> ref (Node (mk_full_quadtreenode n.body n.mass n.quad
                                             (dn_from_node 0 n.nw)
                                             n.ne
                                             (dn_from_node 2 n.se)
                                             (dn_from_node 3 n.sw)))
      | 2 -> ref (Node (mk_full_quadtreenode n.body n.mass n.quad
                                             (dn_from_node 0 n.nw)
                                             (dn_from_node 1 n.ne)
                                             n.se
                                             (dn_from_node 3 n.sw)))
      | 3 -> ref (Node (mk_full_quadtreenode n.body n.mass n.quad
                                             (dn_from_node 0 n.nw)
                                             (dn_from_node 1 n.ne)
                                             (dn_from_node 2 n.se)
                                             n.sw))
      | _ -> failwith "Version limited to 4 Threads only"
    end
      
      (* /!\ Unoptimised/4thread version /!\*)
(*Extract bodies from a quadtree*)
let rec extract_bodies oc3 =
  begin
    match !oc3 with
    | Node n ->
       if is_external n then
         if is_some !(n.body) then
           [(noneSome !(n.body))]
         else
           []
       else 
         (extract_bodies n.nw)@(extract_bodies n.ne)@(extract_bodies n.se)@(extract_bodies n.sw)
    | _ -> []
  end


    (* /!\ Unoptimised/4thread version /!\*)     
    (*Compute forces between bodies*)
    let rec distant_force (body:Body.body) (tree:quadtree ref) =
      let body = ref body in
      begin
        match !tree with
          Empty -> failwith "Empty is not possible ! Oh wait …"
         |Node n ->
           if (is_none (!(n.body))) || Body.equals (noneSome (!(n.body))) body then (
             (0.0,0.0)
           )
           else
             if is_external n then
               begin
                 (*adds the force of n.body to body*)
                 compute_force body (noneSome !(n.body))
               end
             else
               begin
                 let s = n.quad.length in
                 let d = distance !(noneSome !(n.body)) !body in
                 if (s/.d) < Utils.theta then (
                   compute_force body (noneSome !(n.body))
                 )
                 else
                   begin
                     let f1 = distant_force !body n.nw in
                     let f2 = distant_force !body n.ne in
                     let f3 = distant_force !body n.sw in
                     let f4 = distant_force !body n.se in
                     (fst f1 +. fst f2 +. fst f3 +. fst f4,snd f1 +. snd f2 +. snd f3 +. snd f4);
                   end
               end
         | Distantnode n -> (0.0,0.0)
                              
      end

        (* /!\ Unoptimised/4thread version /!\*)     
        (*Add force to bodies*)
        let rec add_force_to_body (body:Body.body) (tree:quadtree ref) (f:(float*float)) =
          begin
            match !tree with
              Empty -> failwith "Empty is not possible ! Oh wait …"
             |Node n ->
               if (is_none (!(n.body))) then
                 ()
               else
                 if Body.equals (noneSome (!(n.body))) (ref body) then (
                   let b = (noneSome (!(n.body))) in
                   !b.fx := (!b).!fx +. (fst f);
                   !b.fy := (!b).!fy +. (snd f);
                 )
                 else
                   if is_external n then
                     begin
                       (*adds the force of n.body to body*)
                       (* compute_force body (noneSome !(n.body)) *)
                       let b = (noneSome (!(n.body))) in
                       !b.fx := (!b).!fx +. (fst f);
                       !b.fy := (!b).!fy +. (snd f);
                     end
                   else
                     begin
                       let s = n.quad.length in
                       let d = distance !(noneSome !(n.body)) body in
                       if (s/.d) < Utils.theta then (
                         (* compute_force body (noneSome !(n.body)) *)
                         let b = (noneSome (!(n.body))) in
                         !b.fx := (!b).!fx +. (fst f);
                         !b.fy := (!b).!fy +. (snd f);
                       )
                       else
                         begin
                           add_force_to_body body n.nw f;
                           add_force_to_body body n.ne f;
                           add_force_to_body body n.sw f;
                           add_force_to_body body n.se f
                         end
                     end
             | Distantnode n -> ()
                                  
          end
