(*Body representation*)
type body = {
    rx:float ref; ry:float ref; (*Position*)
    vx:float ref; vy:float ref; (*Velocity*)
    fx:float ref; fy:float ref; (*Force*)
    mass:float         (*Mass*)
  }

(*Build a body*)
let mk_body rx ry vx vy fx fy mass =
  {rx;ry;vx;vy;fx;fy;mass}

(*Update body position*)
let up_body_r (b:body) (rx:float) (ry:float) =
  b.rx := rx;
  b.ry := ry

(*Update body velocity*)
let up_body_v (b:body) (vx:float) (vy:float) =
  b.vx := vx;
  b.vy := vy

(*Update body force*)
let up_body_f (b:body) (fx:float) (fy:float) =
  b.fx := fx;
  b.fy := fy

(*Reset body force*)
let reset_body_f b =
  up_body_f b 0.0 0.0

(*Build body from a list of values*)
let mk_body_from_lst lst =
  let rx = ref (float_of_string (List.nth lst 0)) in
  let ry = ref (float_of_string (List.nth lst 1)) in
  let vx = ref (float_of_string (List.nth lst 2)) in
  let vy = ref (float_of_string (List.nth lst 3)) in
  let mass = float_of_string (List.nth lst 4) in
  let fx = ref 0.0 in
  let fy = ref 0.0 in
  {rx;ry;vx;vy;fx;fy;mass}

(*Init bodies from a file*)
let init_bodies infile n =
  Array.init n (fun _ ->
               let str = Str.split (Str.regexp "[\ ]") (input_line infile) in
               ref (mk_body_from_lst str))


(*Print the value of a body*)
let print_body (b:body) =
  Printf.printf "Pos(%10.3E %10.3E)  Velocity(%10.3E %10.3E)  Force(%10.3E %10.3E) Mass(%10.3E)\n" !(b.rx) !(b.ry) !(b.vx) !(b.vy) !(b.fx) !(b.fy) b.mass


(*Equality between bodies*)
let equals (body1:body ref) (body2:body ref) =
  !(!body1.rx) = !(!body2.rx) && !(!body1.ry) = !(!body2.ry)
  && !(!body1.vx) = !(!body2.vx) && !(!body1.vx) = !(!body2.vx)
  && !body1.mass = !body2.mass
