open Graphics;;
open Body;;
open Utils;;

(*Window parameters*)
let w_size = 800.0
let half_w_size = w_size /. 2.
                              
(*Input file*)
let infile = 
  let file = Sys.argv.(1) in
  open_in file
          
(*Number of praticles*)
let n = int_of_string (input_line infile)

(*Radius of univers*)
let radius_universe = float_of_string (input_line infile)

(*Compute max coord*)
let get_max_coord bodies =
  let max = ref 0.0 in
  let _ =
    Array.iter
      (fun b ->
        if abs_float !(!b.rx) > !max then max := abs_float !(!b.rx)
        else
          if abs_float !(!b.ry) > !max then max := abs_float !(!b.ry))
      bodies;
  in  
  Printf.printf "borne supp %f\n" !max;
  !max

(*Simulation steps*)
let steps = 100.
(*Current time*)
let t = ref 0.0
(*Time delta*)
let dt = 0.1
           
let _ =
  (*Open display window*)
  open_graph (" "^(string_of_int (int_of_float w_size))^"x"^(string_of_int (int_of_float w_size)));

  (*The bodies of the system*)
  let bodies = init_bodies infile n in

  let max_coord = get_max_coord bodies in
  
  while !t < steps do

    (* Printf.printf "--------------------------------------------\n--------------------------------------------\n"; *)
    
    let quad = (Quadtree.mk_quad 0.0 0.0 (radius_universe *. 2.0)) in
    let qd3 = ref (Quadtree.Node (Quadtree.mk_empty_quadtree quad)) in
    let _ = Quadtree.build_quadtree bodies qd3 in
    
    (*Graphical display*)
    clear_graph();
    set_color red;
    fill_rect (round half_w_size) (round half_w_size) 5 5;
    Visu.print_quadtree !qd3 3 half_w_size (max_coord);


    (*Compute and update bodies*)
    Array.iteri
      (fun i b ->
        (* Body.reset_body_f !b; *)
        (* Printf.printf "Before:\n"; *)
        (* print_body !(bodies.(i)); *)
        Quadtree.update_force b qd3;
        Quadtree.update b dt;
        (* Printf.printf "After:\n"; *)
        (* print_body !(bodies.(i)); *)
        (* Printf.printf "---------------------\n"; *)
      )
      bodies;
    
    (* Array.iter (fun x -> print_body !x) bodies; *)
    (* ignore (read_int()); *)

    t := !t +. dt;
    Utils.wait_ms 100.
  done;
  
