(*Gravitational constant*)
let gravity_cst = 6.67E-11

(*Threshold*)
let theta = 0.5

(*Softening parameter*)
let eps = 30000.
let eps2 = eps *. eps
                    
let round x = int_of_float (floor(x +. 0.5))

let is_none = function | Some _ -> false | None -> true

let is_some = function | Some _ -> true | None -> false

exception None_of_option
                                                    
let noneSome = function | Some x -> x | None -> raise (None_of_option)

let wait_ms milli =
  let sec = milli /. 1000. in
  let tm1 = Unix.gettimeofday () in
  while Unix.gettimeofday () -. tm1 < sec do
    ()
  done
