open Utils;;
open Body;;
  
(*Qadrant*)
type quad = {
    xmid:float; (*center of the quandrant*)
    ymid:float;
    length:float (*quandrant length*)
  }

let mk_quad xmid ymid length =
  {xmid;ymid;length}

let print_quad q =
  Printf.printf "xmid=%f,ymid=%f,len=%f\n" q.xmid q.ymid q.length
              
(*Quadtree*)
type quadtree =
  | Empty
  | Node of quadtreenode
   and
     quadtreenode = {
       body:Body.body ref option ref;
       mass:float; (*Mass of quadtree node*)
       quad:quad;
       nw:quadtree ref;   (*Leaves*)
       ne:quadtree ref;
       sw:quadtree ref;
       se:quadtree ref;
     }


(*Built quadtreenode*)
let mk_quadtreenode mass quad nw ne sw se =
  {body=ref None;mass;quad;nw;ne;sw;se}

let mk_empty_quadtree quad =
  {body=ref None;mass=0.0;quad;nw=ref Empty;ne=ref Empty;sw=ref Empty;se=ref Empty}

(*Test if the node is, basically, a leaf*)
let is_external n =
  !(n.nw) = Empty && !(n.ne) = Empty && !(n.sw) = Empty && !(n.se) = Empty

(*Print tree as a leaf*)
let rec print_quadtree_txt oc3 str =
  begin
    match oc3 with
    | Empty -> Printf.printf "%sEmpty\n" str
    | Node n when (is_external n) ->
       Printf.printf "%sLeaf\n" str
    | Node n ->
       Printf.printf "%sNode\n" str;
       print_quadtree_txt !(n.nw) (str^" ");
       print_quadtree_txt !(n.ne) (str^" ");
       print_quadtree_txt !(n.sw) (str^" ");
       print_quadtree_txt !(n.se) (str^" ");
  end
    
(*Test if a body is inside a quad*)
let within body quad =
  let hlen = quad.length /. 2.0 in
  (!(body.rx) >= quad.xmid -. hlen ) && (!(body.rx) <= quad.xmid +. hlen)
  && (!(body.ry) >= quad.ymid -. hlen) && (!(body.ry) <= quad.ymid +. hlen) 


(*Bulild a new body from two bodies*)
let add_to_body (new_body:Body.body) (current_body:Body.body) =
  let m = current_body.Body.mass +. new_body.Body.mass in
  let x = (!(current_body.rx) *. current_body.Body.mass +. !(new_body.rx) *. new_body.Body.mass) /. m in
  let y = (!(current_body.ry) *. current_body.Body.mass +. !(new_body.ry) *. new_body.Body.mass) /. m in
  let vx = !(current_body.vx) in
  let vy = !(current_body.vy) in
  let out = mk_body (ref x) (ref y) (ref vx) (ref vy) (ref 0.0) (ref 0.0) m in out


(*Build quadrants*)
let  mk_quad_nw q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid -. q.length /. 4.0) (q.ymid +. q.length /. 4.0) (hlen)

let  mk_quad_ne q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid +. q.length /. 4.0) (q.ymid +. q.length /. 4.0) (hlen)

let  mk_quad_sw q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid -. q.length /. 4.0) (q.ymid -. q.length /. 4.0) (hlen)

let  mk_quad_se q =
  let hlen = q.length /. 2.0 in
  mk_quad (q.xmid +. q.length /. 4.0) (q.ymid -. q.length /. 4.0) (hlen)

(*Puts a body within the right quadrant*)
let rec put_body (b:Body.body ref option ref) (tree:quadtree ref) =
  let node = (function | Node n -> n | Empty -> failwith "put_body quadtree fail") (!tree) in
  let b = Utils.noneSome !b in
  let nw = mk_quad_nw node.quad in
  let ne = mk_quad_ne node.quad in
  let sw = mk_quad_sw node.quad in
  let se = mk_quad_se node.quad in
  (*Select the quadrant*)
  if within !b nw then
    insert_quadtree (b) node.nw
  else
    if within !b ne then
      insert_quadtree (b) node.ne
    else
      if within !b se then
        insert_quadtree (b) node.se
      else
        if within !b sw then
          insert_quadtree (b) node.sw

(*Insert a body inside a quadtree*)
and insert_quadtree (body:Body.body ref) (tree:quadtree ref) =
  begin
    match !tree with
      Node n ->
      (*body of node is empty*)
      if is_none !(n.body) then
        begin
          n.body := Some (ref !body)
        end
      else
        (*Internal node*)
        if not (is_external n) then
          begin
            n.body := Some (ref (add_to_body !(noneSome (!(n.body))) !body));
            put_body (ref (Some body)) tree
          end
        else
          begin
            let nw = mk_quad_nw n.quad in
            let ne = mk_quad_ne n.quad in
            let sw = mk_quad_sw n.quad in
            let se = mk_quad_se n.quad in
            n.nw := Node (mk_empty_quadtree nw);
            n.ne := Node (mk_empty_quadtree ne);
            n.se := Node (mk_empty_quadtree se);
            n.sw := Node (mk_empty_quadtree sw);
            put_body n.body tree;
            put_body (ref (Some body)) tree;
            n.body := Some (ref (add_to_body !(noneSome (!(n.body))) !body));
          end
    | Empty -> failwith "Empty is impossible !! Oh wait …"
  end
    
(*Build quadtree from bodies*)
let build_quadtree (bodies:Body.body ref array) (tree:quadtree ref) =
  Array.iter (fun b -> insert_quadtree b tree) bodies;
  tree

(*compute force of body B to body A*)          
let compute_force (a:Body.body ref) (b:Body.body ref) =
  print_body !a;
  print_body !b;
  let dx = !(!b.rx) -. !(!a.rx) in
  let dy = !(!b.ry) -. !(!a.ry) in
  let dist = sqrt (dx *. dx +. dy *. dy) in
  Printf.printf "Dist -> %10.3E\n" dist;
  let force = (Utils.gravity_cst *. !a.Body.mass *. !b.Body.mass) /. (dist *. dist +. eps2) in
  Printf.printf "Force -> %10.3E\n" force;
  let fx = force *. dx /. dist in
  let fy = force *. dy /. dist in
  Printf.printf "(%10.3E,%10.3E)\n" fx fy;
  (fx,fy)
                  
(*add force of body B to body A*)
let add_force (a:Body.body ref) (b:Body.body ref) =
  Printf.printf "    force of :";
  print_body !b;
  let fx,fy = compute_force a b in
  Printf.printf "    -> (%10.3E,%10.3E)\n" fx fy;
  !a.fx := !(!a.fx) +. fx;
  !a.fy := !(!a.fy) +. fy;
  ()
    
(*Distance from a to b*)
let distance (a:Body.body) (b:Body.body) =
  let dx = !(a.rx) -. !(b.rx) in
  let dy = !(a.ry) -. !(b.ry) in
  sqrt (dx*.dx +. dy*.dy)
       
(*Compute forces*)
let rec update_force (body:Body.body ref) (tree:quadtree ref) =
  begin
    match !tree with
     Empty -> failwith "Empty is not possible ! Oh wait …" 
    |Node n ->
      (*Empty node or same node*)
      if (is_none (!(n.body))) || Body.equals (noneSome (!(n.body))) body then (
        ()
      )
      else
        (*Node is external*)
        if is_external n then
          begin
            (*adds the force of n.body to body*)
            add_force body (noneSome !(n.body))
          end
        else
          begin
            (*Check if the quadtree node is not too far*)
            let s = n.quad.length in
            let d = distance !(noneSome !(n.body)) !body in
            if (s/.d) < Utils.theta then (
              add_force body (noneSome !(n.body))
            )
            else
              begin
                update_force body n.nw;
                update_force body n.ne;
                update_force body n.sw;
                update_force body n.se;
              end
          end
  end

(*Update bodies after force calculation*)
let update (body:Body.body ref) (dt:float) =
  !body.vx := !(!body.vx) +. ( dt *. !(!body.fx) /. !body.Body.mass);
  !body.vy := !(!body.vy) +. ( dt *. !(!body.fy) /. !body.Body.mass);
  !body.rx := !(!body.rx) +. (!(!body.vx) *. dt);
  !body.ry := !(!body.ry) +. (!(!body.vy) *. dt)

