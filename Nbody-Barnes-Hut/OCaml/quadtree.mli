type quad = { xmid : float; ymid : float; length : float; }
val mk_quad : float -> float -> float -> quad
val print_quad : quad -> unit
type quadtree = Empty | Node of quadtreenode
and quadtreenode = {
  body : Body.body ref option ref;
  mass : float;
  quad : quad;
  nw : quadtree ref;
  ne : quadtree ref;
  sw : quadtree ref;
  se : quadtree ref;
}
val mk_quadtreenode :
  float ->
  quad ->
  quadtree ref ->
  quadtree ref -> quadtree ref -> quadtree ref -> quadtreenode
val mk_empty_quadtree : quad -> quadtreenode
val is_external : quadtreenode -> bool
val print_quadtree_txt : quadtree -> string -> unit
val within : Body.body -> quad -> bool
val add_to_body : Body.body -> Body.body -> Body.body
val mk_quad_nw : quad -> quad
val mk_quad_ne : quad -> quad
val mk_quad_sw : quad -> quad
val mk_quad_se : quad -> quad
val put_body : Body.body ref option ref -> quadtree ref -> unit
val insert_quadtree : Body.body ref -> quadtree ref -> unit
val build_quadtree : Body.body ref array -> quadtree ref -> quadtree ref
val compute_force : Body.body ref -> Body.body ref -> float * float
val add_force : Body.body ref -> Body.body ref -> unit
val distance : Body.body -> Body.body -> float
val update_force : Body.body ref -> quadtree ref -> unit
val update : Body.body ref -> float -> unit
