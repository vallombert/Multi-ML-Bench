(*Pstr_value*)

let multi f x =
  where node =
    let y = 1 in
    let v = << #y# + #x# >> in
    let rec_call = << f $v$ >> in
    let p = proj rec_call in
    let r = List.fold_right (fun i y -> p i + y) [0;1] 0 in
    r
      where leaf =
      x

let res = f 1

let _ = print_int res;;


(*Pstr_eval*)
let multi g x =
  where node =
    let y = 1 in
    let v = << #y# + #x# >> in
    let rec_call = << g $v$ >> in
    let p = proj rec_call in
    let r = List.fold_right (fun i y -> p i + y) [0;1] 0 in
    r
      where leaf =
      x
    in print_int (g 1)
