type ident = string

type term = Term of ident * term list

(** A total ordering function over terms; To be used for Map, Set, etc. *)
val compare : term -> term -> int

val hash: term -> int

module CmpTerm : 
 sig 
  type t = term 
  val compare : t -> t -> int 
 end

module CmpTermHash : 
 sig 
  type t = term
  val compare : t -> t -> int
  val hash: t -> int 
 end

module CmpTermLriHash :
 sig
  type t = term
  val equal: t -> t -> bool
  val hash: t -> int
 end

(*Add module sig*)
module TermMap :
sig
  type 'a t = 'a Map.Make(CmpTerm).t
  val is_empty : 'a t -> bool
end

(** A Set of Terms *)
module TermSet :
  sig
    type elt = CmpTerm.t
    type t = MySet.Make(CmpTerm).t
    val empty : t
    val is_empty : t -> bool
    val mem : elt -> t -> bool
    val add : elt -> t -> t
    val singleton : elt -> t
    val remove : elt -> t -> t
    val union : t -> t -> t
    val inter : t -> t -> t
    val diff : t -> t -> t
    val compare : t -> t -> int
    val equal : t -> t -> bool
    val subset : t -> t -> bool
    val iter : (elt -> unit) -> t -> unit
    val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val for_all : (elt -> bool) -> t -> bool
    val exists : (elt -> bool) -> t -> bool
    val filter : (elt -> bool) -> t -> t
    val partition : (elt -> bool) -> t -> t * t
    val cardinal : t -> int
    val elements : t -> elt list
    val min_elt : t -> elt
    val max_elt : t -> elt
    val choose : t -> elt
    val rnd_choose: t -> elt
    val split : elt -> t -> t * bool * t
    val find : elt -> t -> elt
    val of_list : elt list -> t
  end

(* module TermHashSet  :
  sig
    type elt = CmpTermHash.t
    type t = HashSet.Make(CmpTermHash).t
    val empty : unit -> t
    val create : int -> elt -> t
    val length : t -> int
    val is_empty : t -> bool
    val rnd_choose: t -> elt
    val capacity : t -> int
    val mem : t -> elt -> bool
    val clear : t -> unit
    val copy : t -> t
    val iter_v : (elt -> unit) -> t -> unit
    val iter : (elt -> unit) -> t -> unit
    val add_unsafe : t -> elt -> unit
    val copy_resize : t -> int -> t
    val resize : t -> elt -> unit
    val add : t -> elt -> unit
    val remove : t -> elt -> unit
    val create_from_list : elt list -> t
    val bucket_lengths : t -> int array
    val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val for_all : (elt -> bool) -> t -> bool
    val exists : (elt -> bool) -> t -> bool
    val keys : t -> elt array
    val equal : t -> t -> bool
    val update : t -> t -> unit
    val diff_update : t -> t -> unit
    val symmetric_diff_update : t -> t -> unit
    val inter_update : t -> t -> unit
    val union : t -> t -> t
    val diff : t -> t -> t
    val symmetric_diff : t -> t -> t
    val inter : t -> t -> t
  end
*)

module TermLRIHashSet  :
 sig
      type elt = CmpTermLriHash.t
      type t
      val create : int -> t
      val clear : t -> unit
      val copy : t -> t
      val add : t -> elt -> unit
      val remove : t -> elt -> unit
      val mem : t -> elt -> bool
      val cardinal : t -> int
      val iter : (elt -> unit) -> t -> unit
      val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
      val rnd_choose: t -> elt

 end

type rule = term * term

(** AST *)
type decl =
    VarDecl of ident list
  | RulesDecl of rule list
  | StartDecl of term

(** A Term Rewriting System is a list of ident, a list of rules and an initial term *)
type trs = ident list * rule list * term

exception Not_A_Valid_File

(** AST to TSR *)
val decl_to_tsr : decl list -> trs

(** Printing terms *)
val print_term : Format.formatter -> term -> unit

val string_of_term : term -> string
val full_string_of_term : term -> string
val id_string_of_term : term -> string
val only_var_string_of_term : term -> string 

val print_set_of_terms : Format.formatter -> TermSet.t -> unit

(** Printing the whole TRS *)
val print_trs :
  Format.formatter -> trs -> unit

(** The successor function *)
val succ : trs -> TermSet.elt -> TermSet.t
val fast_succ : trs -> TermSet.elt -> TermSet.t
(* val fast_hash_succ : trs -> TermHashSet.elt -> TermHashSet.t *)
val fast_list_succ : trs -> term -> term list
val fast_list_succ_doublons : trs -> term -> term list


(** Test if too many time an id appear in the term *)
val acceptable_term : term -> int -> ident -> bool

(** Initialise naming of term, need "start", the initial term *)
val init_naming_start : term -> unit
