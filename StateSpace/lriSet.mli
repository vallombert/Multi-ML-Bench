type 'a t
val create : int -> 'a t
val clear : 'a t -> unit
val copy : 'a t -> 'a t
val resize : ('a -> int) -> 'a t -> unit
val add : 'a t -> 'a -> unit
val remove : 'a t -> 'a -> unit
val mem : 'a t -> 'a -> bool
val cardinal : 'a t -> int
val iter : ('a -> unit) -> 'a t -> unit
val fold : ('a -> 'b -> 'b) -> 'a t -> 'b -> 'b
module type HashedType =
  sig type t val equal : t -> t -> bool val hash : t -> int end
module type S =
  sig
    type elt
    type t
    val create : int -> t
    val clear : t -> unit
    val copy : t -> t
    val add : t -> elt -> unit
    val remove : t -> elt -> unit
    val mem : t -> elt -> bool
    val cardinal : t -> int
    val iter : (elt -> unit) -> t -> unit
    val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val rnd_choose: t -> elt
  end
module Make :
  functor (H : HashedType) ->
    sig
      type elt = H.t
      type t
      val create : int -> t
      val clear : t -> unit
      val copy : t -> t
      val add : t -> elt -> unit
      val remove : t -> elt -> unit
      val mem : t -> elt -> bool
      val cardinal : t -> int
      val iter : (elt -> unit) -> t -> unit
      val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
      val rnd_choose: t -> elt
    end
