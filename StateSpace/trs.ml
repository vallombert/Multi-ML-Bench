open Format

type ident = string

type term = Term of ident * (term list)

(** A total ordering function over terms; To be used for Map, Set, etc. *)

let gen_cmp = compare

let rec map_cmp cmp l1 l2 =
  match (l1, l2) with
    ([], []) -> 0
  | (a1::l1, []) -> 1
  | ([],a2::l2) -> -1
  | (a1::l1, a2::l2) -> let r=(cmp a1 a2) in 
                          if r=0 
                           then map_cmp cmp l1 l2
                          else r
let rec compare t1 t2 = 
 match t1, t2 with
  Term (id1,l1), Term (id2,l2) -> 
    if id1=id2 then (map_cmp compare l1 l2)
     else String.compare id1 id2 

(* let hash = Hashtbl.hash *)

let gen_hash = Hashtbl.hash

(* let hash = gen_hash *)

let hash t = Hashtbl.hash_param 500 500 t 

(*let hash t = match t with
     | Term(id,[]) -> gen_hash id
     | Term(id,hd::tl) -> gen_hash id *)
              

module CmpTerm =
 struct
  type t = term
  let compare = gen_cmp
 end

module CmpTermHash =
 struct
  type t = term
  let compare = gen_cmp
  let hash t = (* gen_hash *) Hashtbl.hash_param 500 500 t 
 end

module CmpTermLriHash =
 struct
  type t = term
  let equal t1 t2 = t1=t2
  let hash t = Hashtbl.hash_param 500 500 t 
 end


module TermSet = MySet.Make(CmpTerm)
module TermMap = Map.Make(CmpTerm)

(* module TermHashSet = HashSet.Make(CmpTermHash) *)

module TermLRIHashSet = LriSet.Make(CmpTermLriHash)

let namingTerm = ref TermMap.empty
let cptTerm = ref 1

type rule = term * term

type decl =
  | VarDecl of ident list
  | RulesDecl of rule list
  | StartDecl of term

type trs = ident list * rule list * term

exception Not_A_Valid_File

(** AST to TSR *)

let is_VarDecl = function VarDecl _ -> true | _ -> false
let is_RulesDecl = function RulesDecl _ -> true | _ -> false
let is_StartDecl = function StartDecl _ -> true | _ -> false

let find_VarDecl d = 
 match List.filter is_VarDecl d with
   | [VarDecl l] -> l
   |  _  -> raise Not_A_Valid_File
  
let find_RulesDecl d = 
 match List.filter is_RulesDecl d with
   | [RulesDecl l] -> l
   |  _  -> raise Not_A_Valid_File

let find_StartDecl d = 
 match List.filter is_StartDecl d with
   | [StartDecl t] -> t
   |  _  -> raise Not_A_Valid_File

let decl_to_tsr d = find_VarDecl d,find_RulesDecl d,find_StartDecl d

(** Printing *)

let print_id fmt id = fprintf fmt "%s" id
let print_id_vars fmt id = fprintf fmt " %s" id

(** Printing terms *)
let rec print_term fmt t =
  match t with
    | Term(id,[]) -> fprintf fmt "%a" print_id id
    | Term(id,t::l) -> 
	fprintf fmt "%a(%a%a)" print_id id 
	  print_term t print_term_list l

and print_term_list fmt l =
  match l with
    | [] -> ()
    | t::l -> fprintf fmt ",%a%a" print_term t print_term_list l


let string_of_term t =
 let size = ref 0 in
 let rec string_of_term t =
  if !size>10 then "..." else
   match t with
     | Term(id,[]) -> incr size;id
     | Term(id,hd::tl) -> incr size;
	id^"("^(string_of_term hd)^(string_of_term_list tl)^")"
and string_of_term_list l =
  if !size>10 then "..." else
  match l with
    | [] -> ""
    | hd::tl -> ","^(string_of_term hd)^(string_of_term_list tl)
 in
 (string_of_term t)

let full_string_of_term t =
 let rec string_of_term t =
   match t with
     | Term(id,[]) -> id
     | Term(id,hd::tl) -> id^"("^(string_of_term hd)^(string_of_term_list tl)^")"
and string_of_term_list l =
  match l with
    | [] -> ""
    | hd::tl -> ","^(string_of_term hd)^(string_of_term_list tl)
 in
 (string_of_term t)

let only_var_string_of_term t =
  let rec string_of_term t =
   match t with
     | Term(id,[]) -> id
     | Term(id,hd::tl) -> (string_of_term hd)^(string_of_term_list tl)
and string_of_term_list l =
  match l with
    | [] -> ""
    | hd::tl -> (string_of_term hd)^(string_of_term_list tl)
 in
 (string_of_term t)



let id_string_of_term t = TermMap.find t !namingTerm


let print_rules fmt l = 
 fprintf fmt "(RULES\n"; 
 List.iter (fun r -> match r with
                      (lhs,rhs) -> fprintf fmt "%a -> %a ;\n" print_term lhs print_term rhs) l;
  fprintf fmt ")\n"
 
let print_vars fmt l = 
  fprintf fmt "(VAR"; List.iter (fun v -> print_id_vars fmt v) l;
  fprintf fmt ")\n"

let print_start fmt t = fprintf fmt "(START\n ";print_term fmt t;fprintf fmt "\n)\n"

let print_set_of_terms fmt set = 
 fprintf fmt "{\n";
 TermSet.iter (fun t-> print_term fmt t) set;
 fprintf fmt "}\n"

(** Printing the whole TRS *)
let print_trs fmt trs =  
 match trs with
  vars,rules,start -> print_vars fmt vars; print_rules fmt rules; print_start fmt start
 

(** Computing REDEX *)

(* ocaml trs.cmo lexer.cmo parser.cmo *)

(*(* Test if a term (of the rules, so the need of the variables) can match a sub-term *)*)
(*let rec matching vars rule t = *)
(* match rule with *)
(*   (* Rule is without any subterm *)*)
(*  | Term (idR,[]) ->  *)
(*     begin *)
(*       if (List.mem idR vars) *)
(*        (* if rule is a variable, it can match every thing *) *)
(*        then true *)
(*       else match t with*)
(*        (* Otherwise, t must be the same constant *)*)
(*             | Term (idT,[]) -> idR=idT*)
(*             | Term (idT,subsT) -> false *)
(*      end*)
(*   (* Rule has subterms *)*)
(*  | Term (idR,subsR) -> *)
(*      match t with*)
(*        (* t is a constant so without subterms and so not matching the rule *)*)
(*       | Term (idT,[]) -> false*)
(*       | Term (idT,subsT) -> (idR=idT) (* same root *)*)
(*                              && ((List.length subsT)=(List.length subsR)) (* same lenght of subterms *)*)
(*                              &&   (* Check if one subterms does not match *)*)
(*                               (List.for_all2 (fun sR sT -> matching vars sR sT) subsR subsT)*)

exception Not_Match

(* Test if a term (of the lhs, so the need of the variables) can match a sub-term can return the mapping, Not_Match otherwise *)
let rec matching vars lhs t = 
 match lhs with 
   (* Lhs is without any subterm *)
  | Term (idR,[]) ->  
     begin 
       if (List.mem idR vars) 
        (* if Lhs is a variable, it can match every thing *) 
        then [(idR,t)]
       else match t with
        (* Otherwise, t must be the same constant *)
             | Term (idT,[]) -> if idR=idT then [(idR,t)] else raise Not_Match
             | Term (idT,subsT) -> raise Not_Match 
      end
   (* Lhs has subterms *)
  | Term (idR,subsR) -> 
      match t with
        (* t is a constant so without subterms and so not matching the lhs *)
       | Term (idT,[]) -> raise Not_Match 
       | Term (idT,subsT) -> if (idR<>idT) (* same root *)
                              || ((List.length subsT)<>(List.length subsR)) (* same lenght of subterms *)
                             then raise Not_Match 
                             else   (* Check if one subterms does not match *)
                               (List.fold_left2 (fun e sR sT -> e@(matching vars sR sT)) [] subsR subsT)

(*let rec fold_left_i i f accu l =*)
(*  match l with*)
(*    [] -> accu*)
(*  | a::l -> fold_left_i (i+1) f (f i accu a) l*)
(*let fold_left_i f accu l = fold_left_i 0 f accu l*)

(* substitute a term from a mapping *)
let rec substitute mapping t =
 match t with
   | Term (id,subs) -> try 
                        List.assoc id mapping 
                       with Not_found -> Term (id, List.map (substitute mapping) subs)


(* Compute all the substitution to apply for a term t; return a list of position and what to substitute *)
let find_redex (vars,all_lhs,all_rhs) t = 
 let to_sub = ref [] in
 let rec find_deep t l = 
   List.iter2 (fun lhs rhs -> 
                try 
                  let new_map=(matching vars lhs t) in 
                    to_sub:=(l,substitute new_map rhs)::!to_sub
                with Not_Match -> ()
               ) all_lhs all_rhs;
    match t with
     | Term (id,[]) -> ()
     | Term (id,subs) -> 
        List.iteri (fun i ts -> find_deep ts (i::l)) subs; 
 in
  find_deep t [];!to_sub

(* substitute a term from a map and a rhs *)
(*let rec substitute map rhs pos t =*)
(* match t with*)
(*   | Term (id,subs) -> *)

(* reduce a term at a position by what need to replace *)
let rec reduce pos tS t =  
 match pos with 
 | []     -> tS
 | hd::tl ->  match t with
                | Term (id,subs) -> Term (id, List.mapi (fun i st -> if i=hd then (reduce tl tS st) else st) subs)

let succ (vars,rules,start) =
 let (vars,all_lhs,all_rhs as theo') = vars,List.map fst rules,List.map snd rules in
  (fun t ->
  List.fold_left 
   (fun setSucc (pos,tWork) -> let new_t=(reduce pos tWork t) in
                                if not(TermMap.mem new_t !namingTerm) 
                                 then 
                                   begin 
                                    incr cptTerm;
                                    namingTerm:=TermMap.add new_t (string_of_int !cptTerm) !namingTerm
                                   end;
                            TermSet.add new_t setSucc) 
    TermSet.empty (find_redex theo' t))

let fast_succ (vars,rules,start) =
 let (vars,all_lhs,all_rhs as theo') = vars,List.map fst rules,List.map snd rules in
  (fun t ->
  List.fold_left 
   (fun setSucc (pos,tWork) -> TermSet.add (reduce pos tWork t) setSucc) 
    TermSet.empty (find_redex theo' t))

(* 

let fast_hash_succ (vars,rules,start) =
 let (vars,all_lhs,all_rhs as theo') = vars,List.map fst rules,List.map snd rules in
  (fun t ->
    let new_e = TermHashSet.empty() in
     List.iter (fun (pos,tWork) -> TermHashSet.add new_e (reduce pos tWork t)) (find_redex theo' t);
     new_e
 )
*)


let fast_list_succ (vars,rules,start) =
 let setAdd e l = if List.exists (fun e'-> (gen_cmp e e')=0) l then l else e::l in
 let (vars,all_lhs,all_rhs as theo') = vars,List.map fst rules,List.map snd rules in
  (fun t ->
    List.fold_left 
      (fun setSucc (pos,tWork) -> setAdd (reduce pos tWork t) setSucc) 
        [] (find_redex theo' t))

let fast_list_succ_doublons (vars,rules,start) =
 let (vars,all_lhs,all_rhs as theo') = vars,List.map fst rules,List.map snd rules in
  (fun t ->
    List.fold_left 
      (fun setSucc (pos,tWork) -> (reduce pos tWork t)::setSucc) 
        [] (find_redex theo' t))
   
 
exception Too_Big

(*let acceptable_term nMax idT t =*)
(* let rec test n t =  *)
(*  if n>nMax then raise Too_Big else*)
(*   match t with*)
(*   | Term (id,subs) -> List.fold_left (fun cpt tt -> test cpt tt) (if idT=id then (n+1) else n) subs*)
(* in*)
(*  try ignore(test 0 t);true with Too_Big -> false*)

let init_naming_start start = namingTerm:= TermMap.add start (string_of_int !cptTerm) !namingTerm

let acceptable_term t nMax idT =
 let rec test n t =  
  if n>nMax then raise Too_Big else
   match t with
   | Term (id,subs) -> let m= (if idT=id then (n+1) else n) in 
                        List.iter (fun tt -> test m tt) subs
 in
  try ignore(test 0 t);true with Too_Big -> false
